# Device Manager 4 Linux

**UPDATE: 27/01/2025**: 
**Orbitiny Desktop Environment** released. The current repository for this device manager is no longer maintained as its got some issues which have been fixed in the new version that comes with the **Orbitiny Desktop Environment**: https://gitea.com/sasko.usinov/orbitiny-desktop.

**UPDATE: 12/05/2023**: This project is somewhat out of date now but it has *not* been forgotten. A few things have changed since my last update, mainly with the backend along with some cosmetical updates and the reason I have not been active here is because I've been working on a new Desktop Environment for Linux (soon to be announced).

Device Manager 4 Linux is a distro agnostic graphical device manager for Linux that lets you disable and enable devices without blacklisting the associated kernel module and without requiring a reboot. 

![Device Manager 4 Linux](screenshots/screenshot_1.png) 
![Device Manager 4 Linux](screenshots/screenshot_2.png)
![Device Manager 4 Linux](screenshots/screenshot_3.png)

You can also apply the changes when your system starts.

The source code is being updated on a daily/hourly bases. Check often if you want to get the latest and most reliable version. Please email me if you find isuses.

I will be releasing AppImages any moment but haven't yet because I am still testing.
