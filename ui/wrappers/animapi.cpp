//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include <QMessageBox>
#include <QDialog>
#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>
#include <QScreen>
#include <QWindow>

#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>

#include "wrappers.h"

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void animapi::slide_widget(QWidget * wdg, slider_struct data)
{
// int screen_index =               screenapi::get_screen_index(wdg);
// QList<QScreen *>  screens =      QGuiApplication::screens();

   wdg->setMinimumWidth(data.min_width);
   wdg->setMaximumWidth(data.max_width);
   wdg->setMinimumHeight(data.min_height);
   wdg->setMaximumHeight(data.max_height);

   QPropertyAnimation * animation = {new QPropertyAnimation(nullptr, "geometry")};
   animation->setTargetObject(wdg);
   animation->setDuration(1500);
   animation->setStartValue(QRect(data.start_x, data.start_y, data.start_width, data.start_height));
   animation->setEndValue(QRect(data.end_x, data.end_y, data.end_width, data.end_height));
   animation->setEasingCurve(QEasingCurve::InOutExpo);
   animation->start(QAbstractAnimation::DeleteWhenStopped);
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void animapi::fade_in_widget(QWidget * wdg)
{
   QGraphicsOpacityEffect * qga = new QGraphicsOpacityEffect(nullptr);
   QPropertyAnimation * anim = new QPropertyAnimation(qga, "opacity");
   anim->setStartValue(0);
   anim->setEndValue(1);
   anim->setEasingCurve(QEasingCurve::OutQuad);
   anim->setDuration(800);
   wdg->setGraphicsEffect(qga);
   anim->start(QPropertyAnimation::DeleteWhenStopped);
}
