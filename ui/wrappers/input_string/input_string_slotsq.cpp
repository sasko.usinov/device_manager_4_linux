//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include "input_string.h"
#include "ui_input_string.h"

#include "lib/libsources/api.h"

#include "ui/wrappers/wrappers.h"

#include <QDesktopWidget>
#include <QScreen>
#include <QFileDialog>


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void input_string::slotq_ok_clicked(bool)
{
   if (!get_input_edit()->text().trimmed().size())
   {
      messages::show_message("No input entered. Please enter a valid input and try again.");
      get_input_edit()->setFocus();

      return;
   }

   close();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void input_string::slotq_cancel_clicked(bool)
{
   get_input_edit()->setText("");

   close();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void input_string::slotq_select_file_clicked(bool)
{
   std::string file_path = QFileDialog::getOpenFileName(this, "File Selection", api::str::get_path_from_url(get_input_edit()->text().toStdString()).c_str(), "").toStdString();

   if (!file_path.size())
      return;

   get_input_edit()->setText(file_path.c_str());

}

