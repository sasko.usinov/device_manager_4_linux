//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#ifndef INPUT_STRING_H
#define INPUT_STRING_H

#include <QDialog>
#include <QTimer>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "ui_input_string.h"


namespace Ui {
class input_string;
}

class input_string : public QDialog
{
   Q_OBJECT

   //------------------------------------------------------------------------------------------------//
   // public section
   //------------------------------------------------------------------------------------------------//
   public:
      explicit                               input_string                           (QWidget *parent, const std::string & caption = "", const std::string & default_text = "", bool file_select_visible = false);
      explicit                               input_string                           (QWidget *parent, std::string msg, int time_out, int duration = 700);
      ~                                      input_string                           ();

      inline QLineEdit *                     get_input_edit                         () {return ui->is_txt_input;}
      inline QPushButton *                   get_ok_button                          () {return ui->is_btn_ok;}
      inline QPushButton *                   get_cancel_button                      () {return ui->is_btn_cancel;}
      inline QLabel *                        get_caption_label                      () {return ui->is_lbl_caption;}
      inline QPushButton *                   get_file_select_button                 () {return ui->is_btn_select_file;}


   //------------------------------------------------------------------------------------------------//
   // private section
   //------------------------------------------------------------------------------------------------//
   private:

      Ui::input_string *                     ui;

      void                                   connect_signals                        ();

   //------------------------------------------------------------------------------------------------//
   // Events
   //------------------------------------------------------------------------------------------------//
      void                                   showEvent                              (QShowEvent *e);
      void                                   keyPressEvent                          (QKeyEvent *e);


   //------------------------------------------------------------------------------------------------//
   // slots:
   //------------------------------------------------------------------------------------------------//
    private slots:

      void                                   slotq_select_file_clicked              (bool);
      void                                   slotq_ok_clicked                       (bool);
      void                                   slotq_cancel_clicked                   (bool);
};


#endif // INPUT_STRING
