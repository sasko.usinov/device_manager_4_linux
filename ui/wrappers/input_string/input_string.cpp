//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include "input_string.h"
#include "ui_input_string.h"

#include "ui/wrappers/wrappers.h"

#include <QDesktopWidget>
#include <QScreen>


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
input_string::input_string(QWidget * parent, const std::string & caption, const std::string & default_text, bool file_select_visible) :
    QDialog(parent),
    ui(new Ui::input_string)
{
   ui->setupUi(this);

   connect_signals();

   if (caption.size())
      get_caption_label()->setText(caption.c_str());

   get_input_edit()->setText(default_text.c_str());
   get_input_edit()->setFocus();
   get_input_edit()->selectAll();

   setFixedHeight(height());
   get_file_select_button()->setVisible(file_select_visible);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
input_string::~input_string()
{
   delete ui;
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void input_string::connect_signals()
{
   connect(get_ok_button(), SIGNAL(clicked(bool)), this, SLOT(slotq_ok_clicked(bool)));
   connect(get_cancel_button(), SIGNAL(clicked(bool)), this, SLOT(slotq_cancel_clicked(bool)));
   connect(get_file_select_button(), SIGNAL(clicked(bool)), this, SLOT(slotq_select_file_clicked(bool)));
// connect(timer, SIGNAL(timeout()), this, SLOT(slot_qt_on_timer_timeout()));
}


