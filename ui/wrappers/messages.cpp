//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include <QMessageBox>
#include <QDialog>
#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>
#include <QScreen>
#include <QWindow>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "input_string/input_string.h"

#include "wrappers.h"

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int messages::show_question(QWidget * parent, const QString & title, const QString & text, int buttons, int icon)
{
   QMessageBox mbx(parent);

   mbx.setWindowTitle(title);
   mbx.setText(text);

   if (icon    == ICON_NONE)        mbx.setIcon(QMessageBox::NoIcon);
   if (icon    == ICON_INFO)        mbx.setIcon(QMessageBox::Information);
   if (icon    == ICON_EXLAIM)      mbx.setIcon(QMessageBox::Warning);
   if (icon    == ICON_QUESTION)    mbx.setIcon(QMessageBox::Question);
   if (icon    == ICON_CRITICAL)    mbx.setIcon(QMessageBox::Critical);

   if (buttons == BTNS_QST_YESNO)        mbx.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
   if (buttons == BTNS_QST_YESNOCANCEL)  mbx.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

   return (mbx.exec());
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(QWidget * parent, const QString & title, const QString & text)
{
// QMessageBox::information(parent, title, text);

   QMessageBox mbx;
   mbx.setWindowTitle(title);
   mbx.setParent(parent);
   mbx.setText(text);
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(QWidget * parent, const QString & text)
{
// QMessageBox::information(parent, qApp->applicationName(), text);

   QMessageBox mbx;
   mbx.setWindowTitle(qApp->applicationName());
   mbx.setParent(parent);
   mbx.setText(text);
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(const QString & title, const QString & text)
{
// QMessageBox::information(NULL, title, text);

   QMessageBox mbx;
   mbx.setWindowTitle(title);
   mbx.setText(text);
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(const QString & text)
{
// QMessageBox::information(NULL, qApp->applicationName(), text);

   QMessageBox mbx;
   mbx.setWindowTitle(qApp->applicationName());
   mbx.setText(text);
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(long long number)
{
// QMessageBox::information(NULL, QApplication::applicationName(), std::to_string(number).c_str());

   QMessageBox mbx;
   mbx.setWindowTitle(qApp->applicationName());
   mbx.setText(std::to_string(number).c_str());
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(const char * txt)
{
   QMessageBox mbx;
   mbx.setWindowTitle(qApp->applicationName());
   mbx.setText(txt);
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void messages::show_message(const std::vector<std::string> & lst, char term)
{
   QMessageBox mbx;
   mbx.setWindowTitle(qApp->applicationName());
   mbx.setText(utils::vector_to_string(lst, term).c_str());
   mbx.setIcon(QMessageBox::Information);
   mbx.setStandardButtons(QMessageBox::Ok);

   mbx.exec();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string messages::get_input_string(const std::string & caption, const std::string & default_input_text, bool show_file_select)
{
   input_string *       dlg                  {new input_string(nullptr, "Profile Name:", default_input_text.c_str(), show_file_select)};
   std::string          out                  {};
   int                  lf_count             {0};

   for (size_t i = 0; i < caption.size(); i++)
   {
      if (caption.c_str()[i] == 10)
         lf_count++;
   }

   dlg->get_caption_label()->setText(caption.c_str());
   dlg->setFixedHeight(dlg->height() + (15 * lf_count));

   dlg->exec();

   if (dlg->get_input_edit()->text().size())
      out = dlg->get_input_edit()->text().toStdString();

   delete dlg;

   return  out;
}
