//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#ifndef MESSAGES_H
#define MESSAGES_H

#include <QWidget>
#include <QString>
#include <QMessageBox>
#include <QAction>
#include <QMenu>
#include <QScreen>

#include <QDrag>
#include <QMimeData>
#include <QUrl>

#include <iostream>
#include <string>
#include <QTreeWidgetItem>

#include "lib/libsources/liblogman/log_manager.h"

   //-----------------------------------------------------------------------------//
   // namespace messages
   //-----------------------------------------------------------------------------//
   namespace messages
   {      
      //------------------------------------------------------------------------------------------------
      // message_buttons enum
      //------------------------------------------------------------------------------------------------
      enum message_buttons
      {
         BTN_YES                             = QMessageBox::Yes,
         BTN_NO                              = QMessageBox::No,
         BTN_OK                              = QMessageBox::Ok,
         BTN_CANCEL                          = QMessageBox::Cancel
      };

      //------------------------------------------------------------------------------------------------
      // message_icons enum
      //------------------------------------------------------------------------------------------------
      enum message_icons
      {
         ICON_NONE                           = QMessageBox::NoIcon,
         ICON_EXLAIM                         = QMessageBox::Warning,
         ICON_CRITICAL                       = QMessageBox::Critical,
         ICON_QUESTION                       = QMessageBox::Question,
         ICON_INFO                           = QMessageBox::Information,
      };

      //------------------------------------------------------------------------------------------------
      // quetsion_types enum
      //------------------------------------------------------------------------------------------------
      enum quetsion_types
      {
            BTNS_QST_YESNO, BTNS_QST_YESNOCANCEL,
      };

      //------------------------------------------------------------------------------------------------
      // wrapper functions
      //------------------------------------------------------------------------------------------------
      void                                   show_message                              (QWidget * parent, const QString & title, const QString & text);
      void                                   show_message                              (const QString & title, const QString & text);
      void                                   show_message                              (QWidget * parent, const QString & text);
      void                                   show_message                              (const QString & text);
      void                                   show_message                              (long long number);
      void                                   show_message                              (const char * txt);
      void                                   show_message                              (const std::vector<std::string> & lst, char term = 10);

      int                                    show_question                             (QWidget * parent, const QString &title, const QString &text, int buttons, int icon = messages::ICON_QUESTION);

      inline void                            show_message                              (std::string & txt) {messages::show_message(txt.c_str());}
      inline void                            show_message                              (const std::string & txt) {messages::show_message(txt.c_str());}

      std::string                            get_input_string                          (const std::string & caption, const std::string & default_input_text = "", bool show_file_select = false);
   }

   //-----------------------------------------------------------------------------//
   // namespace utils
   //-----------------------------------------------------------------------------//
   namespace utils
   {
      typedef void                           (* ptr)                                   (void);

      std::string                            get_resource_content                      (const std::string &res_path);

      QAction *                              create_action                             (std::vector<QAction *> & actions, QMenu * popup, const std::string & caption, QWidget * parent,
                                                                                        const char * member, bool enabled = true, QIcon icon = QIcon(""), bool add_separator = true,
                                                                                        QFont font = QFont(), const QKeySequence shortcut = 0);

      void                                   delete_actions                            (std::vector<QAction *> & actions);
      void                                   delete_menus                              (std::vector<QMenu *> & menus);

      void                                   apply_tree_node_default_font              (QTreeWidgetItem *item, QColor color);

      std::string                            vector_to_string                          (const std::vector<std::string> & list, char term = '\0');
      std::string                            get_css_content                           (const std::string & file_path);
   }

   //-----------------------------------------------------------------------------//
   // desktop api
   //-----------------------------------------------------------------------------//
   namespace desktop
   {
      #define DESKTOP_FILE_EXTENSION         std::string("desktop")
      #define DESKTOP_FILE_SECTION           std::string("Desktop Entry")
      #define DESKTOP_FILE_ICON_NAME         std::string("Icon")
      #define DESKTOP_FILE_NAME              std::string("Name")
      #define DESKTOP_FILE_TYPE              std::string("Type")
      #define DESKTOP_FILE_VERSION           std::string("Version")
      #define DESKTOP_FILE_EXEC              std::string("Exec")
      #define DESKTOP_FILE_GEN_NAME          std::string("GenericName")
      #define DESKTOP_FILE_COMMENT           std::string("Comment")
      #define DESKTOP_FILE_KEYWORDS          std::string("Keywords")
      #define DESKTOP_FILE_CATEGORIES        std::string("Categories")

      std::vector<std::string>               get_icon_paths                            ();
      std::string                            get_desktop_file_command                  (const std::string & desktop_file);
   }

   //-----------------------------------------------------------------------------//
   // namespace screenapi
   //-----------------------------------------------------------------------------//
   namespace screenapi
   {
      QScreen *                              get_current_screen                        (QWidget *);
      QScreen *                              get_screen_at_cursor                      ();
      int                                    get_screen_index                          (QWidget *);
      int                                    get_total_screen_width                    ();
      int                                    get_total_screen_width_up_to              (int screen_index);
      int                                    get_total_screen_height_up_to             (int screen_index);
      QRect                                  get_screen_geometry                       (QWidget * wdg = nullptr);

      extern api::log_manager                p_out, perr;
   }

   //------------------------------------------------------------------------------------------------//
   // Widget animation API
   //------------------------------------------------------------------------------------------------//
   namespace  animapi
   {
      //------------------------------------------------------------------------------------------------//
      // A data struct needed for the animation
      //------------------------------------------------------------------------------------------------//
      struct slider_struct
      {
         inline                              slider_struct() :
                                             start_width(0), start_height(0),
                                             start_x(0), start_y(0), end_width(0),
                                             end_height(0), end_x(0), end_y(0) {}

         int   start_width,
               start_height,
               start_x,
               start_y,
               end_width,
               end_height,
               end_x,
               end_y,
               min_width,
               max_width,
               min_height,
               max_height;
      };

         void                                   slide_widget                           (QWidget * wdg, slider_struct data);
         void                                   fade_in_widget                         (QWidget * wdg);
   }

   //-----------------------------------------------------------------------------//
   // namespace sysapi
   //-----------------------------------------------------------------------------//
   namespace  sysapi
   {
      void                                      quit                                   (const std::string & reason, int err) __attribute__ ((noreturn));
   }

#endif // MESSAGES_H
