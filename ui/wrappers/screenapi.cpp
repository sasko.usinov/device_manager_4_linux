//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include <QMessageBox>
#include <QDialog>
#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>
#include <QScreen>
#include <QWindow>

#include "wrappers.h"

#include "lib/libsources/api.h"
#include "lib/libsources/liblogman/log_manager.h"

#include "log_macros.h"

api::log_manager
      screenapi::p_out,
      screenapi::perr;

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the screen * the widget is on
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
QScreen * screenapi::get_current_screen(QWidget * wdg)
{
   QScreen * screen = QGuiApplication::screenAt(wdg->pos());

   return screen == nullptr ? QGuiApplication::screens().at(0) : screen;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the screen * the cursor is on
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
QScreen * screenapi::get_screen_at_cursor()
{
   return QGuiApplication::screenAt(QCursor::pos());
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the screen geometry of the screen the cursor is on
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
QRect screenapi::get_screen_geometry(QWidget * wdg)
{
   return wdg ? screenapi::get_current_screen(wdg)->geometry() : QRect();
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the screen index / number the widget is on
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
int screenapi::get_screen_index(QWidget * wdg)
{
   if (wdg == nullptr)
   {
      messages::show_message("get_screen_index(): wdg is nullptr");
      return 0;
   }

   QScreen * screen =            get_current_screen(wdg);

   if (screen == nullptr)
   {
      messages::show_message("get_screen_index(): screen is nullptr");
      return 0;
   }

   QList<QScreen *> screens =    QGuiApplication::screens();

   for (int i = 0; i < screens.count(); i++)
   {
      if (screen == screens[i])
         return i;
   }

   return 0;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the total screen width up to screen with screen index
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int screenapi::get_total_screen_width_up_to(int screen_index)
{
   int out =               {0};

   if ((screen_index) > QGuiApplication::screens().count() - 1)
   {
      messages::show_message("screenapi::get_total_screen_width(): Specified screen_index is greater than the number of screens - 1, will use default 0."
                             "\n\nSpecified screen_index: " + std::to_string(screen_index) +
                             "\n\nNumber of available screen indexes: " + std::to_string(QGuiApplication::screens().count() - 1));
      screen_index = 0;
   }

   if ((screen_index) < 0)
   {
      messages::show_message("screenapi::get_total_screen_width(): Specified screen_index is invalid: " + std::to_string(screen_index) + ", will use default 0.");
      screen_index = 0;
   }

   for (int i = 0; i <= screen_index; i++)
      out += QGuiApplication::screens()[i]->availableGeometry().width();

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the total screen height up to screen with screen index
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int screenapi::get_total_screen_height_up_to(int screen_index)
{
   int out =               {0};

   if ((screen_index) > QGuiApplication::screens().count() - 1)
   {
      messages::show_message("screenapi::get_total_screen_width(): Specified screen_index is greater than the number of screens - 1, will use default 0."
                             "\n\nSpecified screen_index: " + std::to_string(screen_index) +
                             "\n\nNumber of available screen indexes: " + std::to_string(QGuiApplication::screens().count() - 1));
      screen_index = 0;
   }

   if ((screen_index) < 0)
   {
      messages::show_message("screenapi::get_total_screen_width(): Specified screen_index is invalid: " + std::to_string(screen_index) + ", will use default 0.");
      screen_index = 0;
   }

   for (int i = 0; i <= screen_index; i++)
      out += QGuiApplication::screens()[i]->availableGeometry().height();

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the total screen width up to screen with screen index
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int screenapi::get_total_screen_width()
{
   int out =               {0};

   for (int i = 0; i < QGuiApplication::screens().count(); i++)
      out += QGuiApplication::screens()[i]->availableGeometry().width();

   return out;
}
