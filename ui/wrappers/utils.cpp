//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include <QMessageBox>
#include <QDialog>
#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>
#include <QScreen>
#include <QWindow>

#include "log_macros.h"

#include "wrappers.h"

#include "lib/libsources/api.h"

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
QAction * utils::create_action(std::vector<QAction * > & actions,
                              QMenu * popup,
                              const std::string & caption,
                              QWidget * parent,
                              const char * member,
                              bool enabled,
                              QIcon icon,
                              bool add_separator,
                              QFont font,
                              const QKeySequence shortcut)
{
   actions.push_back({new QAction(icon, caption.c_str(), parent)});
   popup->addAction(actions[actions.size() - 1]);
   QObject::connect(actions[actions.size() - 1], SIGNAL(triggered()), parent, member);

   actions[actions.size() - 1]->setEnabled(enabled);
   actions[actions.size() - 1]->setShortcut(shortcut);
   actions[actions.size() - 1]->setFont(font);
   actions[actions.size() - 1]->setShortcutVisibleInContextMenu(true);
   actions[actions.size() - 1]->setParent(popup);

   if (!shortcut.isEmpty())   actions[actions.size() - 1]->setShortcutVisibleInContextMenu(true);

   if (add_separator) popup->addSeparator();

   return actions[actions.size() - 1];
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void utils::delete_actions(std::vector<QAction *> & actions)
{
   for (size_t i = 0; i < actions.size(); i++)
   {
      if (actions[i])
         delete actions[i];
   }
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void utils::delete_menus(std::vector<QMenu *> & menus)
{
   for (size_t i = 0; i < menus.size(); i++)
   {
      if (menus[i])
         delete menus[i];
   }
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void utils::apply_tree_node_default_font(QTreeWidgetItem *item, QColor color)
{
   QFont font("Nimbus Sans L");
   font.setBold(true);
   item->setFont(0, font);
   item->setForeground(0, color);
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string utils::get_resource_content(const std::string & res_path)
{
   std::string out;

   QFile file(res_path.c_str());

   if(!file.open(QIODevice::ReadOnly))
       return std::string();

   out = file.readAll().toStdString();

   file.close();

   return out;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Converts a vector string list to std::string
//
// Returns:       The converted string
//
//-------------------------------------------------------------------------------//
std::string utils::vector_to_string(const std::vector<std::string> & list, char term)
{
   std::string out;

   for (long unsigned int i = 0; i < list.size(); i++)
      out += (term != '\0') ? (list[i] + std::string(1, term)) : list[i];

   return out;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string utils::get_css_content(const std::string & file_path)
{
   if (file_path[0] == ':')
      return utils::get_resource_content(file_path);

   return api::fs::load_from_file(file_path);   // no exceptions thrown, if path does not exist, it returns en empty string and the main line is supposed to check the return value
}


