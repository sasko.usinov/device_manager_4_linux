//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#ifndef toast_message_H
#define toast_message_H

#include <QDialog>
#include <QTimer>
#include <QLabel>
#include <QPropertyAnimation>

#include "ui_toast_message.h"

class toast_message;

namespace Ui
{
   class toast_message;
}

extern toast_message *  toaster;

class toast_message : public QDialog
{
   Q_OBJECT

   //------------------------------------------------------------------------------------------------//
   // public section
   //------------------------------------------------------------------------------------------------//
   public:
      explicit                               toast_message                          (QWidget *parent, const std::string &msg, int duration, int speed = 700, bool stay_on_screen = false);
      inline ~                               toast_message                          () {delete ui; toaster = nullptr; if (animation) delete  animation;}

      static inline void                     hide_toast_message                     () {if (toaster) toaster->hide();}
      inline QLabel *                        get_message_label                      () {return ui->u_tm_lbl_message;}

      void                                   show_toast                             (const std::string &msg, int time_out = 4000, int speed = 700);

      inline QTimer *                        get_timer                              () {return tmr_anim;}

   //------------------------------------------------------------------------------------------------//
   // private section
   //------------------------------------------------------------------------------------------------//
   private:
      Ui::toast_message *                    ui;
      QTimer *                               tmr_anim;
      QWidget *                              parent_window;
      QPropertyAnimation *                   animation;

      void                                   start_timer                            (int time_out);
      inline int                             get_size_est                           (int value, int total) {return (total / 100) * value;}

      void                                   install_event_filters                  ();

      //------------------------------------------------------------------------------------------------//
      // evenets
      //------------------------------------------------------------------------------------------------//
      void                                   showEvent                              (QShowEvent *e);
      bool                                   eventFilter                            (QObject * target, QEvent * event);

      //------------------------------------------------------------------------------------------------//
      // data members
      //------------------------------------------------------------------------------------------------//
      bool                                   stay_visible;

   //------------------------------------------------------------------------------------------------//
   // slots:
   //------------------------------------------------------------------------------------------------//
    private slots:
      void                                   slot_qt_on_timer_timeout               ();
};

#endif // toast_message_H
