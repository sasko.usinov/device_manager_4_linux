//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include "toast_message.h"
#include "ui_toast_message.h"

#include "ui/wrappers/wrappers.h"

#include <QDesktopWidget>
#include <QScreen>

toast_message * toaster = nullptr;

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
toast_message::toast_message(QWidget * parent, const std::string & msg, int duration, int speed, bool stay_on_screen) :
    QDialog(parent),
    ui(new Ui::toast_message), tmr_anim(nullptr), parent_window(parent), animation(nullptr), stay_visible(stay_on_screen)
{
   ui->setupUi(this);

   if (toaster != nullptr)   delete toaster;

   toaster = this;

   install_event_filters();

   setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
   setAttribute(Qt::WA_TranslucentBackground, true);

   show_toast(msg, duration, speed);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void toast_message::show_toast(const std::string & msg, int time_out, int speed)
{
// int screen_index =               screenapi::get_screen_index(parent_window);
// QList<QScreen *>  screens =      QGuiApplication::screens();
// QScreen * curr_screen =          screens[screen_index];
   QScreen * curr_screen =          {screenapi::get_current_screen(parent_window)};

   this->setParent(nullptr);

   get_message_label()->setText(msg.c_str());

   animation =                      {new QPropertyAnimation(nullptr, "geometry")};

   int start_width                  {this->width()};
   int start_height                 {this->height()};
   int start_x                      {curr_screen->geometry().x() + (curr_screen->geometry().width() / 2) - this->width() / 2};
   int start_y                      {curr_screen->geometry().y() + (curr_screen->geometry().height()) + height()};

   int end_width                    {this->width()};
   int end_height                   {this->height()};
   int end_x                        {start_x};
   int end_y                        {curr_screen->geometry().y() + (curr_screen->geometry().height() / 2) - this->height()};

   setWindowFlags(Qt::X11BypassWindowManagerHint | Qt::WindowStaysOnTopHint);
   setGeometry(start_x, start_y, width(), height());

   show();
   raise();

   start_timer(time_out);

   if (animation->state() == animation->Running)
      animation->stop();

   animation->setTargetObject(this);
   animation->setDuration(speed);
   animation->setStartValue(QRect(start_x, start_y, start_width, start_height));
   animation->setEndValue(QRect(end_x, end_y, end_width, end_height));
   animation->setEasingCurve(QEasingCurve::InOutExpo);
   animation->start(QAbstractAnimation::KeepWhenStopped);   
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void toast_message::start_timer(int time_out)
{
   if (get_timer())
   {
      get_timer()->stop();
      delete get_timer();
   }

   tmr_anim = new QTimer(this);

   connect(get_timer(), SIGNAL(timeout()), this, SLOT(slot_qt_on_timer_timeout()));
   get_timer()->start(time_out);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void toast_message::install_event_filters()
{
   installEventFilter(this);

   QList<QWidget *> widgets = this->findChildren<QWidget *>();

   for (int i = 0; i < widgets.count(); i++)
   {
      widgets[i]->installEventFilter(this);
      widgets[i]->setMouseTracking(true);
   }
}

