//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#ifndef DEVICE_PROP_H
#define DEVICE_PROP_H

#include "application.h"

#include <QDialog>
#include <QTimer>
#include <QLabel>
#include <QPropertyAnimation>

#include <QPlainTextEdit>

#define  SPACING     10

#include "ui_device_prop.h"

namespace Ui
{
   class device_prop;
}

class device_prop : public QDialog
{
   Q_OBJECT

   //------------------------------------------------------------------------------------------------//
   // public section
   //------------------------------------------------------------------------------------------------//
   public:
      explicit                               device_prop                         (QWidget *parent = nullptr);
      explicit                               device_prop                         (QWidget *parent, std::string msg, int time_out, int duration = 700);
      ~                                      device_prop                         ();

      inline QPlainTextEdit *                get_device_details_box              () {return ui->u_dp_txt_dev_details;}      
      inline QLabel *                        get_header_label                    () {return ui->u_dp_lbl_dev_name;}

   //------------------------------------------------------------------------------------------------//
   // private section
   //------------------------------------------------------------------------------------------------//
   private:

      Ui::device_prop *                      ui;

      void                                   showEvent                           (QShowEvent *);

      QTimer *                               timer;

   //------------------------------------------------------------------------------------------------//
   // slots:
   //------------------------------------------------------------------------------------------------//
    private slots:
};


#endif // DEVICE_PROP_H
