//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::configure_usb_device()
{
   tree_widget_item * item             {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};
   usb_device_struct * data            {static_cast<usb_device_struct *>(item->get_custom_data())};
   std::string action                  {get_usb_config_op() == ACT_DISABLE_DEVICE ? "unbind" : "bind"};
   std::string script_path             {api::fs::generate_unique_filename("/tmp", "usb")};
   std::vector<std::string>            script;

   script.push_back("#!/usr/bin/pkexec /bin/bash");
   script.push_back("sysfs_id=$(grep -il '" + data->vid_did_processed + "' /sys/bus/usb/devices/*/uevent | tail -1)");
   script.push_back("sysfs_id=$(echo $sysfs_id | cut -f6 -d'/')");
   script.push_back("echo $sysfs_id > /sys/bus/usb/drivers/usb/" + action);

   if (!api::fs::save_to_file(script_path, api::vct::to_string(script, '\n')))
   {
      messages::show_message(("An error occured while trying to save: " + script_path).c_str());
      return;
   }

   if (system(("chmod u+x " + script_path).c_str()))
   {
      messages::show_message(("An error occured changing file permissions of: " + script_path).c_str());
      return;
   }

   plog("Waiting for script to finish: " << script_path);
   plog(api::vct::to_string(script, '\n'));

   detach_file_mon();

   if (m_p_thr_cmd_runner) delete m_p_thr_cmd_runner;

   system(std::string("notify-send -u normal -t 8000 -i info \"Initiating Device Configuration Script \" \"Attempting to reconfigure a device.\"").c_str());

   m_p_thr_cmd_runner = new std::thread(&main_window::t_run_shell_script, this, script_path, SCRIPT_TYPE_USB);
   m_p_thr_cmd_runner->detach();

   emit sigq_new_script_thread_started(script_path);
}


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::read_rc_local()
{
   get_system_boot_list()->clear();

   std::string                content        (api::fs::load_from_file("/etc/rc.local"));
   std::vector<std::string>   lst_rc_local   {api::vct::to_vector_string_list(content, 10)};

   for (size_t i = 0; i < lst_rc_local.size(); i++)
   {
      std::string             line           {api::str::trim(lst_rc_local[i])};

      if (api::str::get_char_index(':', 1, line) != std::string::npos)
      {
         if (line[0] == '#')
            continue;

         std::string dev_desc                {api::str::range_copy(api::str::get_char_index(':', 1, line) + 1, api::str::get_char_index(':', 2, line) - 1, line)};
         std::string script_path             {(api::str::trim((api::str::range_copy(0, api::str::get_char_index('#', 1, line) - 1, line))))};

         tree_widget_item * item             {new tree_widget_item(get_system_boot_list())};

         item->setText(0, dev_desc.c_str());
         item->setToolTip(0, (api::fs::load_from_file(script_path) + "\nScript Path: " + script_path).c_str());

         utils::apply_tree_node_default_font(item, CLR_TWI_PROP);

         if (!api::fs::path_exists(script_path))
         {
            QFont font = item->font(0);
            font.setStrikeOut(true);
            item->setFont(0, font);
            item->setToolTip(0, "Path does not exist");
         }

         //-------------------------------------------------------------------------------//
         // Populate the item properties
         //-------------------------------------------------------------------------------//
         boot_entry_struct boot_entry;
         boot_entry.device_desc = dev_desc;
         boot_entry.boot_script_path = script_path;
         boot_entry.entry_line  = line;
         app::boot_list.push_back(boot_entry);

         item->set_custom_data(static_cast<void *>(& app::boot_list[app::boot_list.size() - 1]));
      }
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int main_window::create_rc_local_backup()
{
   std::string backup_path = api::fs::generate_unique_filename(app::data::app_backup_dir, "rc.local");

   return !system(("cp " + PATH_RC_LOCAL + " " + backup_path).c_str());
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::configure_pci_device()
{
   if (is_thread_busy())
   {
      new toast_message(this, "An existing operation is currently being run. Please wait until it's finished before starting a new one.", 2000, 500, true);
      return;
   }

   tree_widget_item *         item            {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};
   std::string                act_file        {(get_pci_config_op() == ACT_DISABLE_DEVICE) ? "unbind" : "bind"};
   std::vector<std::string>   script          {"#!/usr/bin/pkexec /bin/bash"};

   if (!item)
   {
      messages::show_message(("dynamic_cast<tree_widget_item *>(get_enabled_view()->selectedItems()[0]) failed - " + std::string(__func__)).c_str());
      return;
   }

   m_p_pci_dev_info  =                       {static_cast<pci_device_struct *>(item->get_custom_data())};

   //-------------------------------------------------------------------------------//
   //
   // Check to see if the output of "lspci" returned the kernel module
   // of the currently selected device.
   //
   // This section will only run if we failed to find the "Kernel modules" line
   // in the output of lspci
   //
   //-------------------------------------------------------------------------------//
   if (!m_p_pci_dev_info->kernel_module.size())
   {
      messages::show_message("Cannot disable this device because no kernel module was listed by lspci.");
      return;
   }

   //-------------------------------------------------------------------------------//
   // Check to see if we have multiple module entries
   //
   // When we do so, they are separated by commas like this: "module1, module2"
   //-------------------------------------------------------------------------------//
   if (m_p_pci_dev_info->kernel_module.find(",") != std::string::npos)
   {
      std::vector<std::string> lst           {api::vct::to_vector_string_list(m_p_pci_dev_info->kernel_module, std::vector<char>() = {','})};

      //-------------------------------------------------------------------------------//
      // Build a separate "echo" command for each module entry
      //-------------------------------------------------------------------------------//
      for (size_t i = 0; i < lst.size(); i++)
      {
         std::string mdl_name = api::str::trim(api::str::remove_char(',', lst[i]));

         if (api::fs::path_exists("/sys/bus/pci/drivers/" + mdl_name + "/" + act_file))
            script.push_back("\necho \"" + m_p_pci_dev_info->sysfs_id + "\" > /sys/bus/pci/drivers/" + mdl_name + "/" + act_file);
      }
   }
   //-------------------------------------------------------------------------------//
   // This will run if we only have one module listed in the "Kernel modules"
   // output
   //-------------------------------------------------------------------------------//
   else
      script.push_back("echo \"" + m_p_pci_dev_info->sysfs_id + "\" > /sys/bus/pci/drivers/" + m_p_pci_dev_info->kernel_module + "/" + act_file);

   //-------------------------------------------------------------------------------//
   // We are generating a tmp file and will put the contents of our script in it
   //-------------------------------------------------------------------------------//
   std::string shell_file                    {api::fs::generate_unique_filename(app::data::app_log_dir, "dm4l")};

   plog("Waiting for script to finish: " << shell_file);
   plog(api::vct::to_string(script, C_LF));;

   //-------------------------------------------------------------------------------//
   // Try to save the file
   //-------------------------------------------------------------------------------//
   if (!api::fs::save_to_file(shell_file, api::vct::to_string(script, C_LF)))
   {
      messages::show_message(("Failed to generate: " + shell_file + ".\n\nCannot continue.").c_str());
      return;
   }

   //-------------------------------------------------------------------------------//
   // Change the permissions of the script file and run it
   //-------------------------------------------------------------------------------//
   if (system(("chmod u+x " + shell_file).c_str()))
   {
      messages::show_message(("Failed to change permissions of: " + shell_file + "\n\nCannot continue.").c_str());
      return;
   }

   detach_file_mon();

   if (m_p_thr_cmd_runner) delete m_p_thr_cmd_runner;

   system(std::string("notify-send -u normal -t 8000 -i info \"Initiating Device Configuration Script \" \"Attempting to reconfigure a device.\"").c_str());

   m_p_thr_cmd_runner = new std::thread(&main_window::t_run_shell_script, this, shell_file, SCRIPT_TYPE_PCI);
   m_p_thr_cmd_runner->detach();

   emit sigq_new_script_thread_started(shell_file);
}


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int main_window::add_to_rc_local(const std::string & script_path, const std::string & comment)
{
   if (!api::fs::path_exists(PATH_RC_LOCAL))
   {
      messages::show_message(PATH_RC_LOCAL + " does not exist. Looks like your system does not make use of this file and this is where I add references to generated script.\n\nCannot continue without this.");
      return OP_FAIL;
   }

   if (!create_rc_local_backup())
   {
      messages::show_message(("Failed to backup " + PATH_RC_LOCAL + " to " + app::data::app_backup_dir + "\n\nWill not continue.").c_str());
      return OP_FAIL;
   }

// if (system(std::string("[ -x " + PATH_RC_LOCAL + " ]").c_str()))        // [ -x $PATH_name ] does not appear to work with executable scripts

   if (api::cns::run("ls -l " + PATH_RC_LOCAL).c_str()[3] != 'x')
      messages::show_message(("WARNING: Your  \"" + PATH_RC_LOCAL + "\" is not execitable.\n\nNo commands will run on startup.").c_str());

   std::vector<std::string> lst_rc_local     {api::vct::to_vector_string_list((api::fs::load_from_file(PATH_RC_LOCAL)), 10)};

   //-------------------------------------------------------------------------------//
   // Iterate through lst_rc_local and see if the entry we are trying to add
   // already exists
   //-------------------------------------------------------------------------------//
   for (size_t i = 0; i < lst_rc_local.size(); i++)
   {
      //-------------------------------------------------------------------------------//
      // if we find a match, the entry already exist, no need to do anything
      //-------------------------------------------------------------------------------//
      if (api::str::range_copy(0, script_path.size() - 1, api::str::trim(lst_rc_local[i])) == script_path)
      {
         new toast_message(this, "Selected entry already exists.", 4000, 500);
         return OP_NO_ACTION_TAKEN;
      }
   }

   //-------------------------------------------------------------------------------//
   // See where "exit 0" is, we need to append the new entry right above it
   //-------------------------------------------------------------------------------//
   size_t new_entry_point                      {api::vct::left_index_of(lst_rc_local, "exit 0")};

   //-------------------------------------------------------------------------------//
   // If "exit 0" is not found
   //-------------------------------------------------------------------------------//
   if (new_entry_point == API_ERROR_IDX_NOT_FOUND)
      new_entry_point  = (lst_rc_local.size()) ? lst_rc_local.size() - 1 : 0;

   if (messages::show_question(this, "WARNING", std::string("WARNING! THIS can break your Linux installation.\n\nIf things go haywire, all you need to do is edit /etc/rc.local and remove the line reference that contains the following script: " + script_path + ".\n\nDo you want to continue?").c_str(), messages::BTNS_QST_YESNO) == messages::BTN_NO)
      return OP_NO_ACTION_TAKEN;

   std::vector<std::string> script_content   {};
   std::string tmp_script                    {api::fs::generate_unique_filename(app::data::app_log_dir, "rc_local_update")};

   script_content.push_back("#!/usr/bin/pkexec /bin/bash");
   script_content.push_back("sed -i \"" + std::to_string(new_entry_point) + "i " + script_path + " #" + app::about::app_name + ":" + comment + ": | Don't modify anything before the | symbol\" " + PATH_RC_LOCAL);

   if (!api::fs::save_to_file(tmp_script, api::vct::to_string(script_content, '\n')))
   {
      messages::show_message(("Failed to write to: " + tmp_script).c_str());
      return OP_FAIL;
   }

   if (system(("chmod u+x " + tmp_script).c_str()))
   {
      messages::show_message(("Failed to change permissions of: " + tmp_script).c_str());
      return OP_FAIL;
   }

   return !system(tmp_script.c_str());
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::remove_from_rc_local()
{
   if (!create_rc_local_backup())
   {
      messages::show_message(("Failed to backup /etc/rc.local to " + app::data::app_backup_dir + "\n\nWill not continue.").c_str());
      return;
   }

   tree_widget_item * item    {dynamic_cast<tree_widget_item *>(get_system_boot_list()->selectedItems()[0])};

   if (!item)
   {
      messages::show_message(("This failed:\n\ndynamic_cast<tree_widget_item *>(get_system_boot_list()->selectedItems()[0]) @ " + std::string(__func__) + " -- Cannot continue.").c_str());
      return;
   }

   boot_entry_struct *        boot_entry     {static_cast<boot_entry_struct *>(item->get_custom_data())};
   std::vector<std::string>   rc_local       {api::vct::to_vector_string_list(api::fs::load_from_file("/etc/rc.local"), '\n')};
   size_t                     entry_line_idx {std::string::npos};
   std::string                script_file    {api::fs::generate_unique_filename(app::data::app_log_dir, "update_rc_local_remove_entry.sh")};

   if (!rc_local.size())
   {
      messages::show_message(("Failed to load: \"/etc/rc.local\" - Cannot continue."));
      return;
   }

   for (size_t i = 0; rc_local.size(); i++)
   {
      if (api::str::trim(rc_local[i]) == boot_entry->entry_line)
      {
         entry_line_idx = i;
         break;
      }
   }

   if (entry_line_idx == std::string::npos)
   {
      messages::show_message(("Failed to determine the index of the entry to remove in /etc/rc.local. Did you mess with the entry that automatically got added?\n\nAfter you press OK, I will reload the list and you should try again"));
      read_rc_local();
      return;
   }

   std::vector<std::string>   script_content {};

   script_content.push_back("#!/usr/bin/pkexec /bin/bash");

   script_content.push_back("sed -i '" + std::to_string(++entry_line_idx) + "d' /etc/rc.local");

   if (!api::fs::save_to_file(script_file, api::vct::to_string(script_content, '\n')))
   {
      messages::show_message(("Failed to save: "+ script_file + " - Cannot continue.").c_str());
      return;
   }

   if (system(("chmod u+x " + script_file).c_str()))
   {
      messages::show_message(("Failed to change permissions: "+ script_file + "\n\nCannot continue.").c_str());
      return;
   }

   if (system((script_file).c_str()))
   {
      messages::show_message(("Failed to run file: "+ script_file + "\n\nCannot continue.").c_str());
      return;
   }

   read_rc_local();

   new toast_message(this, "Done!", 1200, 500);
}


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::disable_scsi_device()
{
   if (is_thread_busy())
   {
      new toast_message(this, "An existing operation is currently being run. Please wait until it's finished before starting a new one.", 2000, 500, true);
      return;
   }

   std::vector<std::string>   script;
   std::string shell_file     {api::fs::generate_unique_filename(app::data::app_log_dir, "dm4l")};
   std::string dev_file       {"/sys/class/scsi_device/" + get_scsi_hba_id(get_device_list()->selectedItems()[0]) + "/device/delete"};

   if (!api::fs::path_exists(dev_file))
   {
      messages::show_message("\"" + dev_file + "\" does not exist. Cannot disable this device.");
      return;
   }

   script.push_back("#!/usr/bin/pkexec /bin/bash");
   script.push_back("echo 1 > " + dev_file);

   if (!api::fs::save_to_file(shell_file, api::vct::to_string(script, 10)))
   {
      messages::show_message("Failed to write to: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   if (system(("chmod u+x " + shell_file).c_str()))
   {
      messages::show_message("Failed to change permissions of: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   plog("Running: " << shell_file);
   plog(api::vct::to_string(script, 10));

   new toast_message(this, "Running " + shell_file + "\nThis msg will go away when the script completes.", 1200, 500, true);

   detach_file_mon();

   if (m_p_thr_cmd_runner) delete m_p_thr_cmd_runner;

   set_scsi_action_type(ACTION_SCSI_DISABLE_DEVICE);

   system(std::string("notify-send -u normal -t 8000 -i info \"Initiating Device Configuration Script \" \"Attempting to reconfigure a device.\"").c_str());

   m_p_thr_cmd_runner = new std::thread(&main_window::t_run_shell_script, this, shell_file, SCRIPT_TYPE_SCSI);
   m_p_thr_cmd_runner->detach();

   emit sigq_new_script_thread_started(shell_file);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Rescan of attached SCSI devices.
//                The scan will cause any disabled devices to be reattached
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::rescan_scsi_devices()
{
   std::vector<std::string>   scsi_host_lst {api::vct::to_vector_string_list(api::cns::run("ls /sys/class/scsi_host/"), 10)};
   std::vector<std::string>   script_content;
   std::string shell_file     {api::fs::generate_unique_filename(app::data::app_log_dir, "dm4l")};

   script_content.push_back("#!/usr/bin/pkexec /bin/bash");

   for (size_t i = 0; i < scsi_host_lst.size(); i++)
      script_content.push_back("echo \"- - -\" > /sys/class/scsi_host/" + api::str::trim(scsi_host_lst[i]) + "/scan");

   if (!api::fs::save_to_file(shell_file, api::vct::to_string(script_content, 10)))
   {
      messages::show_message("Failed to write to: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   if (system(("chmod u+x " + shell_file).c_str()))
   {
      messages::show_message("Failed to change permissions of: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   plog("Running: " << shell_file);
   plog(api::vct::to_string(script_content, 10));

   new toast_message(this, "Running " + shell_file + "\nThis msg will go away when the script completes.", 1200, 500, true);

   detach_file_mon();

   if (m_p_thr_cmd_runner) delete m_p_thr_cmd_runner;

   set_scsi_action_type(ACTION_SCSI_RESCAN_DEVICES);

   m_p_thr_cmd_runner = new std::thread(&main_window::t_run_shell_script, this, shell_file, SCRIPT_TYPE_SCSI);
   m_p_thr_cmd_runner->detach();
}
