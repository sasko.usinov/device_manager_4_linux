//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>



//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_about_clicked()
{
   std::string txt =    "<b><font size=\"4\">" + app::about::app_friendly_name + " - " + app::about::app_desc + "</font></b><br><br>";
               txt +=   "Copyright Ⓒ - 2019-2020, " + app::about::app_author + "<br><br>";
               txt +=   "Version: " + app::about::app_version + "<br><br>";
               txt +=   "Build Date: " + app::about::app_build_date + "<br><br>";
               txt +=   "Build Time: " + app::about::app_build_time + "<br><br>";
               txt +=   "<b><font color=\"blue\">" + app::about::app_contact + " / ";
               txt +=   app::about::app_website + "</font></b><br><br>";

   messages::show_message(txt.c_str());
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_device_prop_clicked()
{
   tree_widget_item * item          {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};

   if (!item)                       return;

   pci_device_struct * dev_info     {static_cast<pci_device_struct *>(item->get_custom_data())};

   std::vector<std::string> res = app::get_device_prop(dev_info->sysfs_id);

   device_prop * prop = new device_prop(this);
   prop->get_device_details_box()->setPlainText(api::vct::to_string(res, 10).c_str());
   prop->get_device_details_box()->setWordWrapMode(QTextOption::NoWrap);
   prop->get_header_label()->setText(dev_info->device_desc.c_str());
   prop->get_device_details_box()->setTextInteractionFlags(prop->get_device_details_box()->textInteractionFlags() | Qt::TextSelectableByKeyboard);

   prop->exec();

   delete prop;
}



//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_copy_pci_ids_to_clipboard_clicked()
{
   tree_widget_item * item             {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};

   if (item == nullptr)
      return;

   if (item->get_node_type() != NODE_TYPE_PCI_DEVICE_DESC && item->get_node_type() != NODE_TYPE_PCI_DEVICE_PROP)
      return;

   pci_device_struct * data            {static_cast<pci_device_struct *>(item->get_custom_data())};

   qApp->clipboard()->setText(data->dev_desc_vid_did.c_str());

   new toast_message(this, "PCI ID copied Clipboard", 1000);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_copy_usb_ids_to_clipboard_clicked()
{
   tree_widget_item * item             {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};

   if (item == nullptr)
      return;

   if (item->get_node_type() != NODE_TYPE_USB_DEVICE_DESC)
      return;

   usb_device_struct * data            {static_cast<usb_device_struct *>(item->get_custom_data())};

   qApp->clipboard()->setText(data->vid_did_processed.c_str());

   new toast_message(this, "USB ID copied Clipboard", 1000);
}


