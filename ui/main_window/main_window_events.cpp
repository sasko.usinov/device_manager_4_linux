//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"

#include <QTreeWidgetItemIterator>
#include <QList>
#include <QTextEdit>
#include <QTimer>

#include <unistd.h>

#include "ui/toast_message/toast_message.h"

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
bool main_window::eventFilter(QObject * object, QEvent * event)
{
   if (event->type() == QEvent::Type::KeyRelease)
   {
      if (m_s_last_search_term == get_search_input()->text().toStdString())
         return true;

      app::search_for_device(get_search_input()->text().toStdString().c_str());
      m_s_last_search_term = get_search_input()->text().toStdString();

      return true;
   }

   return QDialog::eventFilter(object, event);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::showEvent(QShowEvent *)
{
   //
}
