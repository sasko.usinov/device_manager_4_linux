//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"

#include <QTreeWidgetItemIterator>
#include <QList>
#include <QTextEdit>
#include <QTimer>

#include <unistd.h>

#include "ui/toast_message/toast_message.h"


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::load_pci_list(std::vector<pci_device_struct> & list, bool show_toast_when_done, bool expand_all_when_done)
{
   tree_widget_item * parent_node         {new tree_widget_item(get_device_list())};
   tree_widget_item * dev_type_node       {nullptr};

   parent_node->setText(0, "PCI Devices");
   parent_node->set_node_type(NODE_TYPE_PCI_TOP_LEVEL);
   parent_node->setToolTip(0, api::str::trim(api::str::load_from_file("/etc/hostname")).c_str());

   utils::apply_tree_node_default_font(parent_node, CLR_TWI_PRNT);

   for (size_t i = 0; i < list.size(); i++)
   {
      //-------------------------------------------------------------------------------//
      // Check to see whether the hardware type node already exists
      //
      // E.g: PCI Bridge, Network Controller etc
      //-------------------------------------------------------------------------------//
      QTreeWidgetItemIterator it(get_device_list());

      while (*it)
      {
         if ((*it)->text(0).toStdString() == list[i].device_type)
            dev_type_node = dynamic_cast<tree_widget_item *>((*it));

         ++it;
      }

      //-------------------------------------------------------------------------------//
      // Add a new hardware type node if we haven't found one
      //
      // E.g: PCI Bridge, Network Controller etc
      //
      // otherwise, we will use the one found above
      //-------------------------------------------------------------------------------//
      if (!dev_type_node)
      {
         dev_type_node = new tree_widget_item(parent_node);
         dev_type_node->setText(0, api::str::trim(list[i].device_type).c_str());
//       dev_type_node->set_custom_data(static_cast<device_info_struct *>(& device_list[i]));
         dev_type_node->set_node_type(NODE_TYPE_PCI_PERIPHERAL_TYPE);

         utils::apply_tree_node_default_font(dev_type_node, CLR_TWI_PROP);
      }

      //-------------------------------------------------------------------------------//
      // Add device description - E.g: Copmany XYZ Audio Controller
      // and set its custom data to the current device_info_struct being worked with
      //-------------------------------------------------------------------------------//
      tree_widget_item * dev_desc = new tree_widget_item(dev_type_node);
      dev_desc->setText(0, (list[i].device_desc + " - [" + list[i].dev_desc_vid_did + "]").c_str());
      dev_desc->set_custom_data(static_cast<pci_device_struct *>(& list[i]));
      dev_desc->set_node_type(NODE_TYPE_PCI_DEVICE_DESC);

      utils::apply_tree_node_default_font(dev_desc, CLR_TWI_DESC);

      //-------------------------------------------------------------------------------//
      // Set dev_desc's tool tip
      //-------------------------------------------------------------------------------//
      dev_desc->setToolTip(0, (list[i].dev_desc_vid_did).c_str());

      if (list[i].kernel_module.size())
         dev_desc->setToolTip(0,   (dev_desc->toolTip(0).toStdString() + ":" + list[i].kernel_module).c_str());

      //-------------------------------------------------------------------------------//
      // Tell the user via tooltip() if the device is disabled
      //-------------------------------------------------------------------------------//
      /*
      if (!list[i][i].device_enabled)
         dev_desc->setToolTip(0, dev_desc->toolTip(0) + " - This device is disabled!");
      else
         dev_desc->setToolTip(0, dev_desc->toolTip(0) + " - This device is working properly.");
      */

      if (!list[i].device_enabled)
         dev_desc->setIcon(0, QPixmap(ICON_PATH_HW_DISABLED));

      //-------------------------------------------------------------------------------//
      // Add each property associated with the current device as a child node
      // This is basically the stuff returned by lspci - the property are the lines
      // started with \t under the device description in the output of lspci
      //
      // E.g: Kernel Module, Subsystem etc
      //-------------------------------------------------------------------------------//
      for (size_t prop_idx = 0; prop_idx < list[i].prop.size(); prop_idx++)
      {
         tree_widget_item * prop = new tree_widget_item(dev_desc);

         prop->setText(0, api::str::trim(list[i].prop[prop_idx]).c_str());
         prop->set_custom_data(static_cast<pci_device_struct *>(& list[i]));
         prop->setToolTip(0, dev_desc->toolTip(0));
         prop->set_node_type(NODE_TYPE_PCI_DEVICE_PROP);

         utils::apply_tree_node_default_font(prop, CLR_TWI_PROP);
      }

      //-------------------------------------------------------------------------------//
      // Did we find a kernel module?
      //-------------------------------------------------------------------------------//
      if (!list[i].kernel_module.size())
      {
         tree_widget_item * prop = new tree_widget_item(dev_desc);
         prop->setText(0, TXT_NO_MODULE_FOUND);
         prop->set_custom_data(static_cast<pci_device_struct *>(& list[i]));
         prop->setToolTip(0, dev_desc->toolTip(0));
         prop->set_node_type(NODE_TYPE_PCI_DEVICE_PROP);
         prop->setForeground(0, QColor(255, 0, 0));
      }

      //-------------------------------------------------------------------------------//
      // Add the number of children to the tooltip of the dev_type node
      //-------------------------------------------------------------------------------//
      std::string parent_text = dev_type_node->text(0).toStdString() + " (" + std::to_string(dev_type_node->childCount()) + ")";

      //-------------------------------------------------------------------------------//
      // Add the address of the current dev_desc to its associated device_info_struct::data
      //-------------------------------------------------------------------------------//
      list[i].data = static_cast<tree_widget_item *>(dev_desc);

      dev_type_node->setToolTip(0, parent_text.c_str());

      dev_type_node = nullptr;
   }

   get_device_list()->collapseAll();
   get_device_list()->topLevelItem(0)->setExpanded(true);

   if (show_toast_when_done)
      new toast_message(this, "Device List Successfully Loaded", 1200, 500);

   if (expand_all_when_done)
      slotq_on_expand_all_clicked();
}
