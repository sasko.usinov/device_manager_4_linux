//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::generate_usb_startup_script()
{
   tree_widget_item * item             {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};
   usb_device_struct * data            {static_cast<usb_device_struct *>(item->get_custom_data())};
   std::string action                  {get_usb_config_op() == ACT_DISABLE_DEVICE ? "unbind" : "bind"};
   std::string script_path             {data->boot_script_path};
   std::vector<std::string>            script;

   script.push_back("#!/bin/bash");
   script.push_back("");
   script.push_back("# This script was generated automatically by: " + app::about::app_name);
   script.push_back("");
   script.push_back("# Running this script as su will disable this device.");
   script.push_back("");
   script.push_back("# Invoke this script from within /etc/rc.local");
   script.push_back("");
   script.push_back("# Device Name: " + data->hub_info);
   script.push_back("");

   script.push_back("sysfs_id=$(grep -il '" + data->vid_did_processed + "' /sys/bus/usb/devices/*/uevent | tail -1)");
   script.push_back("sysfs_id=$(echo $sysfs_id | cut -f6 -d'/')");
   script.push_back("echo $sysfs_id > /sys/bus/usb/drivers/usb/" + action);

   if (!api::fs::save_to_file(script_path, api::vct::to_string(script, '\n')))
   {
      messages::show_message(("An error occured while trying to save: " + script_path).c_str());
      return;
   }

   if (system(("chmod u+x " + script_path).c_str()))
   {
      messages::show_message(("An error occured changing file permissions of: " + script_path).c_str());
      return;
   }

   int status = add_to_rc_local(script_path, data->hub_info + "_" + data->actual_dev_details);

   if (status == OP_FAIL)
   {
      messages::show_message("Operation failed.");
      return;
   }

   if (status == OP_SUCCESS)
   {
      new toast_message(this, "Done!", 1200, 500);

      get_main_pager()->setCurrentIndex(1);

      read_rc_local();
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::generate_pci_startup_script()
{
   tree_widget_item * item             {nullptr};

   if (dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])->get_node_type() == NODE_TYPE_PCI_DEVICE_DESC)
      item = dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0]);
   else
      item = dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0]->parent()); // it will alwyas be a NODE_TYPE_DEVICE_PROP when this runs

   //-------------------------------------------------------------------------------//
   // Check the casting...
   //-------------------------------------------------------------------------------//
   if (dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])->get_node_type() == NODE_TYPE_PCI_PERIPHERAL_TYPE)
   {
      messages::show_message(("In " + std::string(__func__) + " - get_node_type() == NODE_TYPE_PERIPHERAL_TYPE. Cannot continue.").c_str());
      return;
   }

   pci_device_struct * dev_info = static_cast<pci_device_struct *>(item->get_custom_data());

   //-------------------------------------------------------------------------------//
   //
   // Check to see if the output of "lspci" returned the kernel module
   // of the currently selected device.
   //
   // This section will only run if we failed to find the "Kernel modules" line
   // in the output of lspci
   //
   //-------------------------------------------------------------------------------//
   if (!dev_info->kernel_module.size())
   {
      messages::show_message("Cannot disable this device because no kernel module was listed by lspci.");
      return;
   }

   std::vector<std::string> script;
   script.push_back("#!/bin/bash");
   script.push_back("");
   script.push_back("# This script was generated automatically by: " + app::about::app_name);
   script.push_back("");
   script.push_back("# Running this script as su will disable this device.");
   script.push_back("");
   script.push_back("# Invoke this script from within /etc/rc.local");
   script.push_back("");
   script.push_back("# Device Name: " + dev_info->device_desc);
   script.push_back("");
   script.push_back("sysfs_id=$(grep -il '" + dev_info->dev_desc_vid_did + "' /sys/bus/pci/devices/*/uevent | tail -1)");
   script.push_back("sysfs_id=$(echo $sysfs_id | cut -f6 -d'/')");

   //-------------------------------------------------------------------------------//
   // Check to see if we have multiple module entries
   //
   // When we do so, they are separated by commas like this: "module1, module2"
   //-------------------------------------------------------------------------------//
   if (dev_info->kernel_module.find(",") != std::string::npos)
   {
      std::vector<std::string> lst     {api::vct::to_vector_string_list(dev_info->kernel_module, std::vector<char>() = {','})};

      //-------------------------------------------------------------------------------//
      // Build a separate "echo" command for each module entry
      //-------------------------------------------------------------------------------//
      for (size_t i = 0; i < lst.size(); i++)
      {
         std::string mdl_name = api::str::trim(api::str::remove_char(',', lst[i]));

         if (api::fs::path_exists("/sys/bus/pci/drivers/" + mdl_name + "/unbind"))
            script.push_back("echo \"$sysfs_id\" > /sys/bus/pci/drivers/" + mdl_name + "/unbind");
      }
   }
   else
      script.push_back("echo \"$sysfs_id\" > /sys/bus/pci/drivers/" + dev_info->kernel_module + "/unbind");

   std::string script_path = dev_info->boot_script_path;

   if (!api::fs::save_to_file(script_path, api::vct::to_string(script, '\n')))
   {
      messages::show_message(("An error occured while trying to save: " + script_path).c_str());
      return;
   }

   if (system(("chmod u+x " + script_path).c_str()))
   {
      messages::show_message(("An error occured changing file permissions of: " + script_path).c_str());
      return;
   }

   int status = add_to_rc_local(script_path, dev_info->device_desc);

   if (status == OP_FAIL)
   {
      messages::show_message("Operation failed.");
      return;
   }

   if (status == OP_SUCCESS)
   {
      new toast_message(this, "Done!", 1200, 500);

      get_main_pager()->setCurrentIndex(1);

      read_rc_local();
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::generate_scsi_startup_script()
{
   std::vector<std::string>   script;
   std::string shell_file     {get_scsi_struct(get_device_list()->selectedItems()[0])->boot_script_path};
   std::string dev_file       {"/sys/class/scsi_device/" + get_scsi_hba_id(get_device_list()->selectedItems()[0]) + "/device/delete"};

   if (!api::fs::path_exists(dev_file))
   {
      messages::show_message("\"" + dev_file + "\" does not exist. Cannot disable this device.");
      return;
   }

   script.push_back("#!/usr/bin/pkexec /bin/bash");
   script.push_back("echo 1 > " + dev_file);

   if (!api::fs::save_to_file(shell_file, api::vct::to_string(script, 10)))
   {
      messages::show_message("Failed to write to: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   if (system(("chmod u+x " + shell_file).c_str()))
   {
      messages::show_message("Failed to change permissions of: \"" + shell_file + "\". Cannot continue.");
      return;
   }

   int status = add_to_rc_local(shell_file, static_cast<scsi_device_struct *>((dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0]))->get_custom_data())->product_info);

   if (status == OP_FAIL)
   {
      messages::show_message("Operation failed.");
      return;
   }

   if (status == OP_SUCCESS)
   {
      new toast_message(this, "Done!", 1200, 500);
      get_main_pager()->setCurrentIndex(1);

      read_rc_local();
   }
}
