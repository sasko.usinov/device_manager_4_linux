//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "application.h"

#include <QDialog>
#include <QSplitter>
#include <QTreeWidget>
#include <QStyleFactory>
#include <QTreeWidget>
#include <QKeyEvent>
#include <QPropertyAnimation>
#include <QGraphicsEffect>
#include <QPushButton>
#include <QMenu>
#include <QTimer>
#include <QClipboard>
#include <QElapsedTimer>

#include <thread>

#include "ui_main_window.h"

#include "ui/device_prop/device_prop.h"

#include "ui/toast_message/toast_message.h"

namespace Ui
{
   class main_window;
}

enum node_types
{
   NODE_TYPE_PCI_TOP_LEVEL = 1,
   NODE_TYPE_PCI_PERIPHERAL_TYPE,
   NODE_TYPE_PCI_DEVICE_DESC,
   NODE_TYPE_PCI_DEVICE_PROP,

   NODE_TYPE_SCSI_TOP_LEVEL,
   NODE_TYPE_SCSI_INDEX_ID,
   NODE_TYPE_SCSI_DEVICE_TYPE,
   NODE_TYPE_SCSI_PROP,

   NODE_TYPE_USB_TOP_LEVEL,
   NODE_TYPE_USB_DEVICE_DESC
};

//------------------------------------------------------------------------------------------------//
// classes
//------------------------------------------------------------------------------------------------//


//------------------------------------------------------------------------------------------------//
// tree_widget_item
//------------------------------------------------------------------------------------------------//
class tree_widget_item : public QObject, public QTreeWidgetItem
{
   Q_OBJECT

   //------------------------------------------------------------------------------------------------//
   // public section
   //------------------------------------------------------------------------------------------------//
   public:
      inline explicit                        tree_widget_item                          (const QStringList &strings, int type = Type) : QTreeWidgetItem(strings, type), __data(nullptr), __node_type(0) {}
      inline explicit                        tree_widget_item                          (QTreeWidget * view, int type = Type) : QTreeWidgetItem(view, type), __data(nullptr), __node_type(0) {}
      inline                                 tree_widget_item                          (QTreeWidget * view, const QStringList &strings, int type = Type) : QTreeWidgetItem(view, strings, type), __data(nullptr), __node_type(0) {}
      inline                                 tree_widget_item                          (QTreeWidget * view, tree_widget_item *after, int type = Type) : QTreeWidgetItem(view, after, type), __data(nullptr), __node_type(0) {}
      inline explicit                        tree_widget_item                          (tree_widget_item * parent, int type = Type) : QTreeWidgetItem(parent, type), __data(nullptr), __node_type(0) {}
      inline                                 tree_widget_item                          (tree_widget_item * parent, const QStringList & strings, int type = Type) : QTreeWidgetItem(parent, strings, type), __data(nullptr), __node_type(0) {}
      inline                                 tree_widget_item                          (tree_widget_item * parent, tree_widget_item * after, int type = Type) : QTreeWidgetItem(parent, after, type), __data(nullptr), __node_type(0) {}
      inline                                 tree_widget_item                          (const tree_widget_item & other) : QTreeWidgetItem(other), __data(nullptr), __node_type(0) {}

      inline void *                          get_custom_data                           ()             {return __data;}
      inline void                            set_custom_data                           (void * data)  {__data = data;}

      inline void                            set_node_type                             (int type)     {__node_type = type;}
      inline int                             get_node_type                             ()             {return __node_type;}

      inline tree_widget_item &              operator=                                 (tree_widget_item & twi)
                                                                                       {QTreeWidgetItem::operator=(twi);
                                                                                        set_node_type(twi.get_node_type());
                                                                                        set_custom_data(twi.get_custom_data());
                                                                                        return *this;}

   //------------------------------------------------------------------------------------------------//
   // private
   //------------------------------------------------------------------------------------------------//
   private:
      void *                                 __data;
      int                                    __node_type;
};

//------------------------------------------------------------------------------------------------//
// main_window
//------------------------------------------------------------------------------------------------//
class main_window : public QDialog
{
   Q_OBJECT

   //------------------------------------------------------------------------------------------------//
   // public section
   //------------------------------------------------------------------------------------------------//
   public:
      //------------------------------------------------------------------------------------------------//
      // class methods
      //------------------------------------------------------------------------------------------------//
      explicit                               main_window                               (QWidget *parent = 0);
      inline ~                               main_window                               () {delete m_p_ui;}

      //-------------------------------------------------------------------------------//
      // accessors
      //-------------------------------------------------------------------------------//
      inline QPushButton *                   get_corner_button                         () {return m_p_btn_corner;}
      inline QTabWidget *                    get_main_pager                            () {return m_p_ui->u_mw_tbw_main;}

      inline QTreeWidget *                   get_device_list                           () {return m_p_ui->u_mw_tw_device_list;}
      inline QTreeWidget *                   get_system_boot_list                      () {return m_p_ui->u_mw_tw_system_boot_list;}

      inline QWidget *                       get_central_widget                        () {return m_p_ui->u_mw_wdg_central;}
      inline QWidget *                       get_stats_bar                             () {return m_p_ui->u_mw_wdg_toolbar;}
      inline QWidget *                       get_device_list_bottom_bar                () {return m_p_ui->u_mw_wdg_bottom;}

      inline QLabel *                        get_device_list_header                    () {return m_p_ui->u_mw_lbl_device_header;}
      inline QLabel *                        get_device_list_footer                    () {return m_p_ui->u_mw_lbl_footer;}

      inline QLabel *                        get_system_boot_footer                    () {return m_p_ui->u_mw_boot_list_warning;}
      inline QLabel *                        get_system_boot_header                    () {return m_p_ui->u_mw_lbl_system_boot_header;}

      inline QLineEdit *                     get_search_input                          () {return m_p_ui->u_mw_txt_search_input;}

      inline QTimer *                        get_timer_clear_file_nodes_buffer         () {return & m_tmr_device_refresh;}
      inline QElapsedTimer *                 get_timer_last_file_node_change_stamp     () {return & m_tmr_file_nodes;}

   //------------------------------------------------------------------------------------------------//
   // private section
   //------------------------------------------------------------------------------------------------//
   private:

      //------------------------------------------------------------------------------------------------//
      // vars
      //------------------------------------------------------------------------------------------------//
      Ui::main_window *                      m_p_ui;
      pci_device_struct *                    m_p_pci_dev_info;

      QPushButton *                          m_p_btn_corner;

      int                                    m_n_pci_action_type;                      // See configure_pci_device()
      int                                    m_n_scsi_action_type;
      int                                    m_n_usb_action_type;

      std::thread *                          m_p_thr_cmd_runner;

      bool                                   m_b_thread_busy;
      bool                                   m_b_ready_for_next_query;

      QTimer                                 m_tmr_device_refresh;
      QElapsedTimer                          m_tmr_file_nodes;

      std::string                            m_s_last_search_term;

      std::vector<std::string>               m_vl_device_info;
      std::vector<std::string>               m_vl_file_nodes;

      api::log_manager                       m_cout, m_cerr;

      void                                   register_meta_types                       ();

      //------------------------------------------------------------------------------------------------//
      // methods
      //------------------------------------------------------------------------------------------------//
      void                                   configure_gui                             ();
      void                                   setup_default_geometry                            ();
      void                                   configure_tree_view                       (QTreeWidget * twdg, const std::string & header_aption);
      void                                   connect_signals                           ();

      void                                   configure_pci_device                      ();
      void                                   disable_scsi_device                       ();
      void                                   configure_usb_device                      ();

      void                                   generate_pci_startup_script               ();
      void                                   generate_scsi_startup_script              ();
      void                                   generate_usb_startup_script               ();

      int                                    add_to_rc_local                           (const std::string & script_path, const std::string & comment);
      void                                   remove_from_rc_local                      ();
      int                                    create_rc_local_backup                    ();

      std::string                            get_scsi_hba_id                           (QTreeWidgetItem * item);
      scsi_device_struct *                   get_scsi_struct                           (QTreeWidgetItem * item);
      void                                   rescan_scsi_devices                       ();

      void                                   init_device_list                          ();
      void                                   refresh_device_list                       ();

      void                                   show_usb_device_prop                      ();      
      std::string                            get_usb_sysfs_id                          (usb_device_struct * usb);

      void                                   load_pci_list                             (std::vector<pci_device_struct> & list, bool show_toast_when_done = true, bool expand_all_when_done = false);
      void                                   load_scsi_list                            (std::vector<scsi_device_struct> & list, bool show_toast_when_done = true, bool expand_all_when_done = false);
      void                                   load_usb_list                             (std::vector<usb_device_struct> & list, bool show_toast_when_done = true, bool expand_all_when_done = false);

      void                                   read_rc_local                             ();

      inline int                             get_pci_config_op                         () {return m_n_pci_action_type;}
      inline void                            set_pci_config_op                         (int op) {m_n_pci_action_type = op;}

      inline int                             get_usb_config_op                         () {return m_n_usb_action_type;}
      inline void                            set_usb_config_op                         (int op) {m_n_usb_action_type = op;}

      inline int                             get_scsi_action_type                      () {return m_n_scsi_action_type;}
      inline void                            set_scsi_action_type                      (int act_type) {m_n_scsi_action_type = act_type;}
      inline bool                            is_thread_busy                            () {return m_b_thread_busy;}

      void                                   install_event_filters                     ();

      void                                   install_kernel_module                     ();

      void                                   attach_file_mon                           ();
      void                                   detach_file_mon                           ();
      void                                   configure_logging                         ();

      //------------------------------------------------------------------------------------------------//
      // threaded methods...
      //------------------------------------------------------------------------------------------------//
      void                                   t_run_shell_script                        (const std::string & script_path, int script_type);
      void                                   t_show_device_info                        (const std::string & file_path);

      //------------------------------------------------------------------------------------------------//
      // events
      //------------------------------------------------------------------------------------------------//
      inline void                            keyPressEvent                             (QKeyEvent *e) {if (e->key() != Qt::Key_Escape) QDialog::keyPressEvent(e);}
      bool                                   eventFilter                               (QObject* obj, QEvent * event);
      void                                   showEvent                                 (QShowEvent *);

   //------------------------------------------------------------------------------------------------//
   // sigc slots
   //------------------------------------------------------------------------------------------------//
      inline void                            slotc_search_pci                          (std::vector<pci_device_struct> & list)
                                                                                       {load_pci_list(list, true, get_search_input()->text().size());}

      inline void                            slotc_search_scsi                         (std::vector<scsi_device_struct> & list)
                                                                                       {load_scsi_list(list, true, get_search_input()->text().size());}

      inline void                            slotc_search_usb                          (std::vector<usb_device_struct> & list)
                                                                                       {load_usb_list(list, true, get_search_input()->text().size());}

      inline void                            slotc_clear_device_list                   () {get_device_list()->clear();}

      void                                   slotc_file_added                          (const std::string & file_path);
      void                                   slotc_file_removed                        (const std::string & file_path);

      void                                   slotc_dev_query_end                       (const std::string & data);

   //------------------------------------------------------------------------------------------------//
   // qt slots
   //------------------------------------------------------------------------------------------------//
   public slots:

      void                                   slotq_init_device_list                    ();

      void                                   slotq_on_pci_script_completed             (int);
      void                                   slotq_on_scsi_script_completed            (int);
      void                                   slotq_on_usb_script_completed             (int);

      inline void                            slotq_on_refresh_device_list              () {refresh_device_list();}

      void                                   slotq_on_new_std_thread_started           (const std::string &);

      void                                   slotq_on_device_list_context_menu         (const QPoint &);
      void                                   slotq_on_boot_list_context_menu           (const QPoint &);

      inline void                            slotq_on_configure_pci_device_clicked     () {configure_pci_device();}
      inline void                            slotq_on_configure_usb_device_clicked     () {configure_usb_device();}
      inline void                            slotq_on_disable_scsi_device_clicked      () {disable_scsi_device();}

      inline void                            slotq_on_refresh_hardware_list_clicked    () {init_device_list();}

      inline void                            slotq_on_disable_pci_on_boot_clicked      () {generate_pci_startup_script();}
      inline void                            slotq_on_disable_scsi_on_boot_clicked     () {generate_scsi_startup_script();}
      inline void                            slotq_on_disable_usb_on_boot_clicked      () {generate_usb_startup_script();}

      inline void                            slotq_on_remove_from_boot_list_clicked    () {remove_from_rc_local();}

      inline void                            slotq_on_open_scripts_dir_clicked         () {system(("xdg-open \"" + app::data::app_scripts_dir + "\"").c_str());}
      inline void                            slotq_on_open_backup_dir_clicked          () {system(("xdg-open \"" + app::data::app_backup_dir + "\"").c_str());}

      inline void                            slotq_on_read_rc_local_clicked            () {read_rc_local(); new toast_message(this, "Done!", 4000, 500);}

      void                                   slotq_corner_button_clicked               ();

      void                                   slotq_device_prop_clicked                 ();
      inline void                            slotq_on_usb_device_prop_clicked          () {show_usb_device_prop();}

      inline void                            slotq_on_about_qt_clicked                 () {qApp->aboutQt();}

      inline void                            slotq_on_copy_to_clipboard_clicked        () {qApp->clipboard()->setText(get_device_list()->selectedItems()[0]->text(0));
                                                                                           new toast_message(this, "Selection has been copied to Clipboard", 3000, 500);}

      inline void                            slotq_on_force_rescan_scsi_clicked        () {rescan_scsi_devices();}

      inline void                            slotq_on_expand_all_clicked               () {slotq_on_collapse_all_clicked();
                                                                                           get_device_list()->expandAll();
                                                                                           get_main_pager()->setCurrentIndex(0);}

      inline void                            slotq_on_collapse_all_clicked             () {get_device_list()->collapseAll();
                                                                                           get_device_list()->topLevelItem(0)->setExpanded(true);
                                                                                           get_main_pager()->setCurrentIndex(0);}

      inline void                            slotq_on_install_new_module_clicked       () {install_kernel_module();}

      inline void                            slotq_on_device_list_item_expanded        (QTreeWidgetItem * item) {get_device_list()->scrollToItem(item->child(0));}

      void                                   slotq_on_dev_query_end                    (const std::string &);
      void                                   slotq_on_file_added                       (const std::string &);
      void                                   slotq_on_file_removed                     (const std::string &);

      void                                   slotq_about_clicked                       ();
      void                                   slotq_on_timeout_start_search             ();
      void                                   slotq_on_timeout_clear_event_buffer       ();

      void                                   slotq_copy_pci_ids_to_clipboard_clicked   ();
      void                                   slotq_copy_usb_ids_to_clipboard_clicked   ();

      //------------------------------------------------------------------------------------------------//
      // resolved by qt
      //------------------------------------------------------------------------------------------------//
      inline void                            on_u_mw_tbw_main_currentChanged           (int index) {if (index == 1) read_rc_local();}
      inline void                            on_u_mw_txt_search_input_textChanged      (const QString &) {}


   //------------------------------------------------------------------------------------------------//
   // qt signals
   //------------------------------------------------------------------------------------------------//
   signals:

      void                                   sigq_dev_query_end                        (const std::string &);
      void                                   sigq_file_added                           (const std::string &);
      void                                   sigq_file_removed                         (const std::string &);

      void                                   sigq_refresh_device_list                  ();

      void                                   sigq_pci_script_completed                 (int);
      void                                   sigq_scsi_script_completed                (int);
      void                                   sigq_usb_script_completed                 (int);

      void                                   sigq_thread_completed                     (int exit_code);
      void                                   sigq_new_script_thread_started            (const std::string &);
};

#endif // MAIN_WINDOW_H
