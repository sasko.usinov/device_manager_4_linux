//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_device_list_context_menu(const QPoint & point)
{
   if (!get_device_list()->selectedItems().size())
      return;

   std::vector<QAction *>           actions;
   tree_widget_item * item          {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};
   QPoint global_pos                {get_device_list()->mapToGlobal(point)};
   QMenu *  popup_menu              {new QMenu(this)};

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   if (!item)                       return;

   //-------------------------------------------------------------------------------//
   // Parent node has been clicked (the one representing the device type
   // E.g: "Network Controller"
   //-------------------------------------------------------------------------------//
   if (item->get_node_type() == NODE_TYPE_PCI_TOP_LEVEL        ||
       item->get_node_type() == NODE_TYPE_PCI_PERIPHERAL_TYPE  ||
       item->get_node_type() == NODE_TYPE_USB_TOP_LEVEL        ||
       item->get_node_type() == NODE_TYPE_SCSI_TOP_LEVEL       ||
       item->get_node_type() == NODE_TYPE_SCSI_INDEX_ID)
   {
      utils::create_action(actions, popup_menu, "Refresh Device List", this, SLOT(slotq_on_refresh_hardware_list_clicked()));

      if (item->get_node_type() == NODE_TYPE_SCSI_TOP_LEVEL || item->get_node_type() == NODE_TYPE_SCSI_INDEX_ID)
         utils::create_action(actions, popup_menu, "Force Rescan SCSI Devices", this, SLOT(slotq_on_force_rescan_scsi_clicked()));
   }

   //-------------------------------------------------------------------------------//
   // PCI Children nodes have been clicked
   //-------------------------------------------------------------------------------//
   if (item->get_node_type() == NODE_TYPE_PCI_DEVICE_DESC || item->get_node_type() == NODE_TYPE_PCI_DEVICE_PROP)
   {
      pci_device_struct * dev_info  {static_cast<pci_device_struct *>(item->get_custom_data())};

      set_pci_config_op(dev_info->device_enabled ? ACT_DISABLE_DEVICE : ACT_ENABLE_DEVICE);

      utils::create_action(actions, popup_menu, std::string((dev_info->device_enabled ? "Disable Device" : "Enable Device")).c_str(), this, SLOT(slotq_on_configure_pci_device_clicked()));
      utils::create_action(actions, popup_menu, "Device Properties", this, SLOT(slotq_device_prop_clicked()));
      utils::create_action(actions, popup_menu, "Refresh Device List", this, SLOT(slotq_on_refresh_hardware_list_clicked()));
      utils::create_action(actions, popup_menu, "Disable on System Boot", this, SLOT(slotq_on_disable_pci_on_boot_clicked()));

      //-------------------------------------------------------------------------------//
      // ...
      //-------------------------------------------------------------------------------//
      (dev_info->device_enabled) ?  plog("Device is enabled: "  << dev_info->device_desc) :
                                    plog("Device is disabled: " << dev_info->device_desc);
   }

   //-------------------------------------------------------------------------------//
   // SCSI Children nodes have been clicked
   //-------------------------------------------------------------------------------//
   if (item->get_node_type() == NODE_TYPE_SCSI_PROP)
   {
      utils::create_action(actions, popup_menu, "Disable Device", this, SLOT(slotq_on_disable_scsi_device_clicked()));
      utils::create_action(actions, popup_menu, "Disable on System Boot", this, SLOT(slotq_on_disable_scsi_on_boot_clicked()));
      utils::create_action(actions, popup_menu, "Refresh Device List", this, SLOT(slotq_on_refresh_hardware_list_clicked()));
      utils::create_action(actions, popup_menu, "Rescan SCSI Devices", this, SLOT(slotq_on_force_rescan_scsi_clicked()));
   }


   //-------------------------------------------------------------------------------//
   // USB Children nodes have been clicked
   //-------------------------------------------------------------------------------//
   if (item->get_node_type() == NODE_TYPE_USB_DEVICE_DESC)
   {
      usb_device_struct * dev_info  {static_cast<usb_device_struct *>(item->get_custom_data())};

      set_usb_config_op(dev_info->device_enabled ? ACT_DISABLE_DEVICE : ACT_ENABLE_DEVICE);

      utils::create_action(actions, popup_menu, dev_info->device_enabled ? "Disable Device" : "Enable Device", this, SLOT(slotq_on_configure_usb_device_clicked()));

      utils::create_action(actions, popup_menu, "Device Properties", this, SLOT(slotq_on_usb_device_prop_clicked()));
      utils::create_action(actions, popup_menu, "Refresh Device List", this, SLOT(slotq_on_refresh_hardware_list_clicked()));
      utils::create_action(actions, popup_menu, "Disable on System Boot", this, SLOT(slotq_on_disable_usb_on_boot_clicked()));
   }

   utils::create_action(actions, popup_menu, "Copy Selection to Clipboard", this, SLOT(slotq_on_copy_to_clipboard_clicked()));

   if (popup_menu)
   {
      popup_menu->setStyleSheet(utils::get_resource_content(CSS_POPUP).c_str());
      popup_menu->exec(global_pos);

      delete popup_menu;
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_boot_list_context_menu(const QPoint & point)
{
   if (!get_system_boot_list()->selectedItems().size())
      return;

   std::vector<QAction *> actions   {};
   QMenu *  popup_menu              {new QMenu(this)};
   QPoint global_pos                {get_system_boot_list()->mapToGlobal(point)};

   utils::create_action(actions, popup_menu, "Remove from System Boot", this, SLOT(slotq_on_remove_from_boot_list_clicked()), get_system_boot_list()->selectedItems().size());
   utils::create_action(actions, popup_menu, "Open rc.local Backup Directory", this, SLOT(slotq_on_open_backup_dir_clicked()));
   utils::create_action(actions, popup_menu, "Open Start-Up Scripts Directory", this, SLOT(slotq_on_open_scripts_dir_clicked()));
   utils::create_action(actions, popup_menu, "Rescan /etc/rc.local and Refresh List", this, SLOT(slotq_on_read_rc_local_clicked()));

   if (popup_menu)
   {
      popup_menu->setStyleSheet(utils::get_resource_content(CSS_POPUP).c_str());
      popup_menu->exec(global_pos);

      delete popup_menu;
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_corner_button_clicked()
{
   std::vector<QAction *> actions   {};
   QMenu * popup_menu               {new QMenu(this)};

   utils::create_action(actions, popup_menu, "Expand Device List", this, SLOT(slotq_on_expand_all_clicked()));
   utils::create_action(actions, popup_menu, "Collapse Device List", this, SLOT(slotq_on_collapse_all_clicked()));
   utils::create_action(actions, popup_menu, "Install New Module", this, SLOT(slotq_on_install_new_module_clicked()), false);
   utils::create_action(actions, popup_menu, std::string("About Qt"), this, SLOT(slotq_on_about_qt_clicked()));
   utils::create_action(actions, popup_menu, std::string("About " + app::about::app_friendly_name).c_str(), this, SLOT(slotq_about_clicked()));

   if (popup_menu)
   {
      popup_menu->setStyleSheet(utils::get_resource_content(CSS_POPUP).c_str());
      popup_menu->exec(QCursor::pos());
      delete popup_menu;
   }
}
