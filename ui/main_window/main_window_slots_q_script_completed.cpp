//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_pci_script_completed(int exit_code)
{
   attach_file_mon();

   std::string search_text          {get_search_input()->text().toStdString()};
   bool set_text                    {api::str::trim(search_text).size() > 0};
   std::string action_type          {(get_pci_config_op() == ACT_ENABLE_DEVICE) ? std::string("enabled") : "disabled"};

   toast_message::hide_toast_message();
   m_b_thread_busy = false;

   if (exit_code)
   {
      messages::show_message(("Operation failed.\n\nOperation returned: " + std::to_string(exit_code)).c_str());
      return;
   }

   new toast_message(this, "Done!", 1200, 500);

   refresh_device_list();

   if (set_text)
      get_search_input()->setText(search_text.c_str());

   if (api::str::trim(search_text).size())
      app::search_for_device(search_text.c_str());

   if (SHOW_CONFIG_NOTIFY)
   {
      if (m_p_pci_dev_info)
         system(std::string("notify-send -u normal -t 8000 -i info \"Hardware Change Detected\" \"A device attached on your system has been " + action_type + ".\n\nAlthough operation has succeeded, these changes are temporary and will be gone upon reboot. To make changes permanent, Disable on System Boot.\n\nAffected device is: " + m_p_pci_dev_info->device_desc + "\"").c_str());
      plog("m_p_pci_dev_info is nullptr, this was not expected and not sure how/whi it is...");
   }
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_scsi_script_completed(int exit_code)
{
   attach_file_mon();

   toast_message::hide_toast_message();
   m_b_thread_busy = false;

   if (exit_code)
   {
      messages::show_message(("Operation failed.\n\nOperation returned: " + std::to_string(exit_code)).c_str());
      return;
   }

   new toast_message(this, "Done!", 1200, 500);

   refresh_device_list();

   if (get_scsi_action_type() == ACTION_SCSI_RESCAN_DEVICES)
      messages::show_message("It may take a few seconds for newly discovered devices to be detected.\n\nIf you don't see the new devices after you click OK, please \"Refresh Device List\" a second or third time.");

   refresh_device_list();

   set_scsi_action_type(ACTION_SCSI_NONE);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_usb_script_completed(int exit_code)
{
   attach_file_mon();

   toast_message::hide_toast_message();
   m_b_thread_busy = false;

   if (exit_code)
   {
      messages::show_message(("Operation failed.\n\nOperation returned: " + std::to_string(exit_code)).c_str());
      return;
   }

   new toast_message(this, "Done!", 1200, 500);

   refresh_device_list();
}


