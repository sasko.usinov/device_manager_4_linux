//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"

#include <QTreeWidgetItemIterator>
#include <QList>
#include <QTextEdit>
#include <QTimer>

#include <unistd.h>

#include "ui/toast_message/toast_message.h"


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::load_usb_list(std::vector<usb_device_struct> &list, bool show_toast_when_done, bool expand_all_when_done)
{
   tree_widget_item * parent_node         {new tree_widget_item(get_device_list())};
   tree_widget_item * dev_type_node       {nullptr};

   parent_node->setText(0, "USB Devices");
   parent_node->set_node_type(NODE_TYPE_USB_TOP_LEVEL);
   parent_node->setToolTip(0, api::str::trim(api::str::load_from_file("/etc/hostname")).c_str());
   parent_node->setIcon(0, QPixmap(ICON_PATH_USB));

   utils::apply_tree_node_default_font(parent_node, CLR_TWI_PRNT);

   if (!app::lsusb_found())
   {
      parent_node->setText(0, parent_node->text(0) + " - lsusb (required but not found) - Please install it (usbutils)");
      return;
   }

   for (size_t i = 0; i < list.size(); i++)
   {
      //-------------------------------------------------------------------------------//
      // Check to see whether the hardware type node already exists
      //
      // E.g: PCI Bridge, Network Controller etc
      //-------------------------------------------------------------------------------//
      QTreeWidgetItemIterator it(get_device_list());

      while (*it)
      {
         if ((*it)->text(0).toStdString() == list[i].device_type)
            dev_type_node = dynamic_cast<tree_widget_item *>((*it));

         ++it;
      }

      //-------------------------------------------------------------------------------//
      // Add a new hardware type node if we haven't found one
      //
      // E.g: PCI Bridge, Network Controller etc
      //
      // otherwise, we will use the one found above
      //-------------------------------------------------------------------------------//
      if (!dev_type_node)
      {
         dev_type_node = new tree_widget_item(parent_node);
         dev_type_node->setText(0, api::str::trim(list[i].device_type).c_str());
//       dev_type_node->set_custom_data(static_cast<device_info_struct *>(& device_list[i]));
         dev_type_node->set_node_type(NODE_TYPE_USB_TOP_LEVEL);

         utils::apply_tree_node_default_font(dev_type_node, CLR_TWI_PROP);
      }

      tree_widget_item * dev_desc = new tree_widget_item(dev_type_node);
      dev_desc->setText(0, std::string(list[i].device_type + " - " + list[i].hub_info + " - \"" + list[i].actual_dev_details + "\"" + " - [" + list[i].vid_did + "]").c_str());
      dev_desc->set_node_type(NODE_TYPE_USB_DEVICE_DESC);
      dev_desc->setToolTip(0, list[i].vid_did.c_str());

      dev_desc->set_custom_data(static_cast<void *>( & list[i]));

      if (!list[i].device_enabled)
         dev_desc->setIcon(0, QPixmap(ICON_PATH_HW_DISABLED));

      utils::apply_tree_node_default_font(dev_desc, CLR_TWI_DESC);

      dev_type_node = nullptr;
   }

   get_device_list()->collapseAll();
   get_device_list()->topLevelItem(0)->setExpanded(true);

   if (show_toast_when_done)
      new toast_message(this, "Device List Successfully Loaded", 1200, 500);

   if (expand_all_when_done)
      slotq_on_expand_all_clicked();
}








