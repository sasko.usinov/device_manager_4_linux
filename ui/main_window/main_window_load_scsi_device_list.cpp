//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"

#include <QTreeWidgetItemIterator>
#include <QList>
#include <QTextEdit>
#include <QTimer>

#include <unistd.h>

#include "ui/toast_message/toast_message.h"


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::load_scsi_list(std::vector<scsi_device_struct> & list, bool show_toast_when_done, bool expand_all_when_done)
{
   tree_widget_item * parent_node         {new tree_widget_item(get_device_list())};
   tree_widget_item * scsi_dev_type       {nullptr};
   size_t last_section_index              {std::string::npos};
   std::vector<tree_widget_item *> lst    {};

   parent_node->setText(0, "SCSI Devices");
   parent_node->set_node_type(NODE_TYPE_SCSI_TOP_LEVEL);
   parent_node->setToolTip(0, api::str::trim(api::str::load_from_file("/etc/hostname")).c_str());

   lst.push_back(parent_node);

   utils::apply_tree_node_default_font(parent_node, CLR_TWI_PRNT);

   if (!app::lshw_found())
   {
      parent_node->setText(0, "lshw (required but not found)");
      return;
   }

   for (size_t i = 0; i < list.size(); i++)
   {
      tree_widget_item * scsi_idx_id_node {new tree_widget_item(parent_node)};
      scsi_idx_id_node->setText(0, std::string("SCSI IDX: " + list[i].scsi_idx).c_str());
      scsi_idx_id_node->set_custom_data(static_cast<scsi_device_struct *>(& list[i]));
      scsi_idx_id_node->set_node_type(NODE_TYPE_SCSI_INDEX_ID);

      lst.push_back(scsi_idx_id_node);

      utils::apply_tree_node_default_font(scsi_idx_id_node, CLR_TWI_PROP);

      for (size_t x = 1; x < list[i].props.size(); x++)
      {
         std::string trimmed              {api::str::trim(list[i].props[x])};

         if (trimmed[0] == '*')
         {
            std::string & line            {list[i].props[x]};

            size_t current_aster_index    {api::str::get_char_index('*', 1, line)};

            if ((current_aster_index > last_section_index) || (current_aster_index < last_section_index))
            {
               scsi_dev_type =            {new tree_widget_item(lst[lst.size() - 1])};
               scsi_dev_type->setText(0, std::string((api::str::range_copy(2, trimmed.size() - 1, trimmed).c_str()) + std::string(" - [") + list[i].hba_number + std::string("]")).c_str());
//             scsi_dev_type->setText(0, std::string(lst[lst.size() - 1]->text(0).toStdString() + " / " + (app::stdapi::str::range_copy(2, trimmed.size() - 1, trimmed).c_str())).c_str());
               scsi_dev_type->set_node_type(NODE_TYPE_SCSI_PROP);
               scsi_dev_type->set_custom_data(static_cast<scsi_device_struct *>(& list[i]));

               lst.push_back(scsi_dev_type);

               utils::apply_tree_node_default_font(scsi_dev_type, CLR_TWI_DESC);
            }
            else if (current_aster_index == last_section_index)
            {
               scsi_dev_type =            {new tree_widget_item(lst[lst.size() - 2])};
               scsi_dev_type->setText(0, std::string(api::str::range_copy(2, trimmed.size() - 1, trimmed) + std::string(" - [") + list[i].hba_number + std::string("]") ).c_str());
               scsi_dev_type->set_node_type(NODE_TYPE_SCSI_PROP);
               scsi_dev_type->set_custom_data(static_cast<scsi_device_struct *>(& list[i]));

               lst.push_back(scsi_dev_type);

               utils::apply_tree_node_default_font(scsi_dev_type, CLR_TWI_DESC);
            }

            x++;
            trimmed =                     {api::str::trim(list[i].props[x])};

            last_section_index =          (api::str::get_char_index('*', 1, line));
         }

         if (scsi_dev_type)
         {
            tree_widget_item * prop       {new tree_widget_item(scsi_dev_type)};
            prop->setText(0, trimmed.c_str());
            prop->set_node_type(NODE_TYPE_SCSI_PROP);
            prop->set_custom_data(static_cast<scsi_device_struct *>(& list[i]));

            utils::apply_tree_node_default_font(prop, CLR_TWI_PROP);
         }
      }

      scsi_dev_type = nullptr;
   }

   get_device_list()->collapseAll();
   get_device_list()->topLevelItem(0)->setExpanded(true);

   if (show_toast_when_done)
      new toast_message(this, "Device List Successfully Loaded", 1200, 500);

   if (expand_all_when_done)
      slotq_on_expand_all_clicked();
}
