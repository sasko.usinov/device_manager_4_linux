//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"

#include <QTreeWidgetItemIterator>
#include <QList>
#include <QTextEdit>
#include <QTimer>
#include <QShortcut>

#include <unistd.h>

#include "ui/toast_message/toast_message.h"


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
main_window::main_window(QWidget *parent) :
   QDialog(parent), m_p_ui(new Ui::main_window),
   m_p_btn_corner(new QPushButton(this)),
   m_p_thr_cmd_runner(nullptr), m_b_thread_busy(false),
   m_b_ready_for_next_query(true)
{
   m_p_ui->setupUi(this);

   configure_logging();

   configure_gui();

   setup_default_geometry();

   connect_signals();

   install_event_filters();

   register_meta_types();

   attach_file_mon();

   init_device_list();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::configure_gui()
{
   setWindowOpacity(0);

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   configure_tree_view(get_device_list(), api::str::trim("Hostname: " + api::str::load_from_file("/etc/hostname")) + " - " + std::string(TXT_TAG_LINE));
   configure_tree_view(get_system_boot_list(), api::str::trim("Hostname: " + api::str::load_from_file("/etc/hostname")) + " - " + std::string(TXT_SYSTEM_BOOT_TITLE));

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   get_device_list()->setIconSize(QSize(ICON_SIZE, ICON_SIZE));
   get_device_list()->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
   get_device_list()->header()->setVisible(false);
   get_device_list_header()->setText((api::str::trim(std::string(TXT_WINDOW_TITLE)).c_str()));
   get_device_list_footer()->setText((api::str::trim(std::string(TXT_TAG_LINE)).c_str()));

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   get_system_boot_list()->setIconSize(QSize(ICON_SIZE, ICON_SIZE));
   get_system_boot_list()->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
   get_system_boot_list()->header()->setVisible(false);
   get_system_boot_header()->setText((std::string(TXT_SYSTEM_BOOT_TITLE)).c_str());
   get_system_boot_footer()->setText((std::string(TXT_BOOT_HELP_MSG) + "\nor you can restore an rc.local backup from: " + app::data::app_backup_dir).c_str());

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   get_main_pager()->setCornerWidget(get_corner_button());
   get_corner_button()->setStyleSheet(utils::get_resource_content(CSS_CORNER_WIDGET).c_str());
   get_corner_button()->setText("...");
   get_corner_button()->setFocusPolicy(Qt::FocusPolicy::NoFocus);

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   setWindowTitle(TXT_WINDOW_TITLE);

   get_search_input()->setFocus();

// (dynamic_cast<QWidget *>(get_search_input()->parent()))->setVisible(false);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::setup_default_geometry()
{
   move(QCursor::pos());

   int new_x = screenapi::get_screen_at_cursor()->geometry().x() + (screenapi::get_screen_at_cursor()->geometry().width() / 2) - (width() / 2);
   int new_y = screenapi::get_screen_at_cursor()->geometry().y() + (screenapi::get_screen_at_cursor()->geometry().height() / 2) - (height() / 2);

   move(new_x, new_y);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::connect_signals()
{
   connect(get_device_list(), SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(slotq_on_device_list_context_menu(const QPoint &)));
   connect(get_device_list(), SIGNAL(itemExpanded(QTreeWidgetItem *)), this, SLOT(slotq_on_device_list_item_expanded(QTreeWidgetItem *)));

   connect(get_system_boot_list(), SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(slotq_on_boot_list_context_menu(const QPoint &)));
   connect(get_corner_button(), SIGNAL(clicked()), this, SLOT(slotq_corner_button_clicked()));

   connect(this, SIGNAL(sigq_new_script_thread_started(const std::string &)), this, SLOT(slotq_on_new_std_thread_started(const std::string &)));
   connect(this, SIGNAL(sigq_pci_script_completed(int)), this, SLOT(slotq_on_pci_script_completed(int)));
   connect(this, SIGNAL(sigq_scsi_script_completed(int)), this, SLOT(slotq_on_scsi_script_completed(int)));
   connect(this, SIGNAL(sigq_usb_script_completed(int)), this, SLOT(slotq_on_usb_script_completed(int)));

   connect(this, SIGNAL(sigq_dev_query_end(const std::string &)), this, SLOT(slotq_on_dev_query_end(const std::string &)));
   connect(this, SIGNAL(sigq_file_added(const std::string &)), this, SLOT(slotq_on_file_added(const std::string &)));
   connect(this, SIGNAL(sigq_file_removed(const std::string &)), this, SLOT(slotq_on_file_removed(const std::string &)));

   connect(this, SIGNAL(sigq_refresh_device_list()), this, SLOT(slotq_on_refresh_device_list()));

   //-------------------------------------------------------------------------------//
   // WARNING: Do not connect app::sigc_conn_file_event from here, causes instability
   //-------------------------------------------------------------------------------//

   app::sigc_load_pci_list.connect(sigc::mem_fun(this, &main_window::slotc_search_pci));
   app::sigc_load_scsi_list.connect(sigc::mem_fun(this, &main_window::slotc_search_scsi));
   app::sigc_load_usb_list.connect(sigc::mem_fun(this, &main_window::slotc_search_usb));

   app::sigc_clear_device_list.connect(sigc::mem_fun(this, &main_window::slotc_clear_device_list));

   connect(get_timer_clear_file_nodes_buffer(), SIGNAL(timeout()), this, SLOT(slotq_on_timeout_clear_event_buffer()));

   connect(new QShortcut(QKeySequence("F7"), this), SIGNAL(activated()), this, SLOT(slotq_copy_pci_ids_to_clipboard_clicked()));
   connect(new QShortcut(QKeySequence("F8"), this), SIGNAL(activated()), this, SLOT(slotq_copy_usb_ids_to_clipboard_clicked()));
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::configure_tree_view(QTreeWidget * twdg, const std::string & header_aption)
{
   twdg->setColumnCount(1);

   QStringList list;
   list.append(header_aption.c_str());
   twdg->setHeaderLabels(list);

   twdg->setAnimated(true);
   twdg->setColumnWidth(0, DEFAULT_COL_SIZE);

   twdg->header()->setDefaultAlignment(Qt::AlignCenter | Qt::AlignVCenter);
   twdg->header()->setVisible(true);
   twdg->header()->setVisible(true);
   twdg->header()->setMinimumHeight(DEFAULT_HDR_SIZE);
}


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::refresh_device_list()
{
   get_search_input()->clear();
   get_device_list()->clear();

   app::load_pci_device_list();
   app::load_scsi_device_list();
   app::load_usb_device_list();

   load_pci_list(app::pci_list);
   load_scsi_list(app::scsi_list);
   load_usb_list(app::usb_list);

   get_timer_last_file_node_change_stamp()->restart();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::init_device_list()
{
   new toast_message(this, "Enumerating devices. Please wait...", 0, 700, true);
   QTimer::singleShot(1000, this, SLOT(slotq_init_device_list()));
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string main_window::get_scsi_hba_id(QTreeWidgetItem * item)
{
   tree_widget_item * twi              {dynamic_cast<tree_widget_item *>(item)};
// scsi_device_struct * ssi            {static_cast<scsi_device_struct *>(twi->get_custom_data())};   // "The scope of the variable 'ssi' can be reduced"
//                                                                                                    // ...but in this case it would be a bad idea

   if (twi->get_node_type() > NODE_TYPE_PCI_DEVICE_PROP)
      return api::str::replace_char('.', ':', static_cast<scsi_device_struct *>(twi->get_custom_data())->hba_number); // bus info

   return std::string();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
scsi_device_struct * main_window::get_scsi_struct(QTreeWidgetItem * item)
{
   tree_widget_item * twi              {dynamic_cast<tree_widget_item *>(item)};

   return static_cast<scsi_device_struct *>(twi->get_custom_data());
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::install_event_filters()
{   
   QList<QWidget *> widgets = this->findChildren<QWidget *>();

   for (int i = 0; i < widgets.count(); i++)
      widgets[i]->installEventFilter(this);
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::show_usb_device_prop()
{
   tree_widget_item * item             {dynamic_cast<tree_widget_item *>(get_device_list()->selectedItems()[0])};
   usb_device_struct * data            {static_cast<usb_device_struct *>(item->get_custom_data())};
   std::vector<std::string> lsusb      {api::vct::to_vector_string_list(api::cns::run("lsusb -v"), std::vector<char>() = {10}, false)};
   std::vector<std::string> prop_list  {api::vct::extract_section(lsusb, "Bus " + data->bus_idx + " Device " + data->device_idx + ":")};

   std::vector<std::string> hwinfo     {api::vct::to_vector_string_list(api::cns::run("hwinfo"), std::vector<char>()  = {10}, false)};
// std::vector<std::string> prop_hwi   {app::get_device_prop(get_usb_sysfs_id(data))};
   std::vector<std::string> prop_hwi   {app::get_device_prop(data->sysfs_id)};

   if (!data->vid_did_processed.size())
   {
      messages::show_message("Cannot obtain device properties for this device.");
      return;
   }

   plog("lsusb: Bus " + data->bus_idx + ", Device " + data->device_idx + ", sysfs_id " << data->sysfs_id);

   prop_list.insert(prop_list.begin() + 0, "=============================\n");
   prop_list.insert(prop_list.begin() + 0, "=============================");
   prop_list.insert(prop_list.begin() + 1, "[lsusb]");

   device_prop * prop = new device_prop(this);
   prop->get_device_details_box()->setPlainText((api::vct::to_string(prop_hwi, 10) + "\n" +  api::vct::to_string(prop_list, 10)).c_str());
   prop->get_device_details_box()->setWordWrapMode(QTextOption::NoWrap);
   prop->get_header_label()->setText(data->hub_info.c_str());
   prop->get_device_details_box()->setTextInteractionFlags(prop->get_device_details_box()->textInteractionFlags() | Qt::TextSelectableByKeyboard);

   prop->exec();

   delete prop;     
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string main_window::get_usb_sysfs_id(usb_device_struct * usb)
{
   std::string script_path             {api::fs::generate_unique_filename("/tmp", "usb")};
   std::vector<std::string> script     {};

   script.push_back("#!/bin/bash");
   script.push_back("sysfs_id=$(grep -il '" + usb->vid_did_processed + "' /sys/bus/usb/devices/*/uevent | tail -1)");
   script.push_back("sysfs_id=$(echo $sysfs_id | cut -f6 -d'/')");
   script.push_back("echo $sysfs_id");

   if (!api::fs::save_to_file(script_path, api::vct::to_string(script, '\n')))
   {
      messages::show_message(("An error occured while trying to save: " + script_path).c_str());
      return std::string();
   }

   if (system(("chmod u+x " + script_path).c_str()))
   {
      messages::show_message(("An error occured changing file permissions of: " + script_path).c_str());
      return std::string();
   }

   return {api::str::trim(api::cns::run(script_path))};
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::attach_file_mon()
{
   //-------------------------------------------------------------------------------//
   // these signals get disconnected whenever an Enable/Disable device op is performed
   //
   // Once the thread completes, we call this method to reconnect the signal
   //-------------------------------------------------------------------------------//

   plog("Enter");

   app::sigc_conn_file_added     = app::sigc_file_added.connect(sigc::mem_fun(this, &main_window::slotc_file_added));
   app::sigc_conn_file_removed   = app::sigc_file_removed.connect(sigc::mem_fun(this, &main_window::slotc_file_removed));

   plog("Exit");
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::detach_file_mon()
{
   plog("Enter");

   app::sigc_conn_file_added.disconnect();
   app::sigc_conn_file_removed.disconnect();

   plog("Exit");
}


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::register_meta_types()
{
   qRegisterMetaType<QVector<int>>("QVector<int>");
   qRegisterMetaType<std::string>("std::string");
   qRegisterMetaType<std::string>("std::size_t");
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::configure_logging()
{
   m_cout.set_log_level(api::log_manager::level_std);
   m_cerr.set_log_level(api::log_manager::level_err);
}
