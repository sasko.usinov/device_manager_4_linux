//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRactions[actions.size() - 1], TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "main_window.h"
#include "ui_main_window.h"

#include "ui/wrappers/wrappers.h"
#include "ui/toast_message/toast_message.h"

#include <QTreeWidgetItemIterator>
#include <QScrollBar>

#include <unistd.h>


//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_dev_query_end(const std::string &)
{

}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_file_added(const std::string & file_path)
{
   plog("File added: " << file_path);

   get_timer_clear_file_nodes_buffer()->start(3000);

   for (size_t i = 0; i < m_vl_file_nodes.size(); i++)
   {
      if (m_vl_file_nodes[i] == file_path)
         return;
   }

   m_vl_file_nodes.push_back(file_path);

   app::show_notification("Hardware Reconfiguration Detected", "A device has been attached.\n\n" + file_path, 8000);

   app::data::thread_list.push_back(std::thread(&main_window::t_show_device_info, this, file_path));
   app::data::thread_list[app::data::thread_list.size() - 1].join();
}

//-------------------------------------------------------------------------------//
// Type:          class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
void main_window::slotq_on_file_removed(const std::string & file_path)
{
   plog("File removed: " << file_path);

   app::show_notification("Hardware Reconfiguration Detected", "A device has been removed.\n\n" + file_path, 4000);

   if (get_timer_last_file_node_change_stamp()->restart() < 250)
      return;

   emit sigq_refresh_device_list();
}
