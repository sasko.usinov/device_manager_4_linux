#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define CSS_POPUP             ":/styles/css/popup_menu.css"
#define CSS_SPLITTER          ":/styles/css/sidebar_splitter.css"
#define CSS_CORNER_WIDGET     ":/styles/css/corner_widget.css"

#define ICON_PATH_HW_DISABLED ":/icons/png/icon_disable.png"
#define ICON_PATH_USB         ":/icons/png/icon_usb.png"

#define __init__              main

#define RUN_EULA              1

#define C_LF                  10
#define C_CR                  13
#define C_HTAB                9
#define ICON_SIZE             10

#define SEARCH_START_TIMEOUT  300
#define SHOW_CONFIG_NOTIFY    1

#define DEFAULT_COL_SIZE      200
#define DEFAULT_HDR_SIZE      30

#define  WATCHER_BUFF_SIZE    1024
#define  DEF_NOTIFY_DELAY     8000

#define TXT_FONT_FACE         "Nimbus Sans L"
#define TXT_WINDOW_TITLE      "Device Manager for Linux"
#define TXT_TAG_LINE          "Disable / Enable Devices Instantly without Rebooting / Blacklisting / Unblacklisting Modules"
#define TXT_SYSTEM_BOOT_TITLE "Devices Disabled on System Boot"
#define TXT_BOOT_HELP_MSG     "If your system fails to boot, use a Live ISO and remove these references from /etc/rc.local"

#define DIRNAME_SCRIPTS       "scripts"
#define DIRNAME_BACKUP        "backups"

#define TXT_EULA_CONTENT      "BY USING THIS SOFTWARE YOU INDICATE/ACCEPT THAT IN NO EVENT SHALL THE DEVELOPER OF THIS SOFTWARE, THE SOURCE CODE OWNER, THE DISTRIBUTOR OR THE COPYRIGHT HOLDER BE HELD LIABLE FOR ANY DIRECT OR INDIRECT DATA LOSS, DAMAGES, INJURIES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS OR DAMAGE ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE."
#define TXT_EULA_ACCEPTED     "eula_accepted"
#define TXT_NO_MODULE_FOUND   "--- NO KERNEL MODULE FOUND ---"
#define TXT_LINE_PADDING      std::string("   ")

#define CLR_TWI_PRNT          QColor(0, 0, 0)
#define CLR_TWI_PROP          QColor(128, 128, 128)
#define CLR_TWI_DESC          QColor(82, 148, 226)

#define PATH_RC_LOCAL         std::string("/etc/rc.local")

#define PROJECT_ID            "dm4l"

#define                       __run__ main_window w; w.show(); return a.exec();

#define SHELL_PATH            std::string("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin")

#endif // DEFINITIONS_H
