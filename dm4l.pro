#-------------------------------------------------
#
# Project created by QtCreator 2019-09-02T19:25:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dm4l
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        application.cpp \
        bootstrap.cpp \
        lib/libsources/cns.cpp \
        lib/libsources/fs.cpp \
        lib/libsources/ini.cpp \
        lib/libsources/liblogman/log_manager.cpp \
        ./ui/main_window/main_window.cpp \
        lib/libsources/str.cpp \
        lib/libsources/sys.cpp \
        lib/libsources/vct.cpp \
        ui/device_prop/device_prop.cpp \
        ui/main_window/main_window_device_config.cpp \
        ui/main_window/main_window_events.cpp \
        ui/main_window/main_window_install_kernel_module.cpp \
        ui/main_window/main_window_load_pci_device_list.cpp \
        ui/main_window/main_window_load_scsi_device_list.cpp \
        ui/main_window/main_window_load_usb_device_list.cpp \
        ui/main_window/main_window_slots_c_file_ops.cpp \
        ui/main_window/main_window_slots_q_file_ops.cpp \
        ui/main_window/main_window_slots_q_general.cpp \
        ui/main_window/main_window_slots_q_popup_builder.cpp \
        ui/main_window/main_window_slots_q_script_completed.cpp \
        ui/main_window/main_window_slots_q_threads_std.cpp \
        ui/main_window/main_window_slots_q_timers.cpp \
        ui/main_window/main_window_startup_script_gen.cpp \
        ui/main_window/main_window_threads_std.cpp \
        ui/toast_message/toast_message.cpp \
        ui/toast_message/toast_message_events_qt.cpp \
        ui/toast_message/toast_message_slots_qt.cpp \
        ui/wrappers/animapi.cpp \
        ui/wrappers/input_string/input_string.cpp \
        ui/wrappers/input_string/input_string_events.cpp \
        ui/wrappers/input_string/input_string_slotsq.cpp \
        ui/wrappers/messages.cpp \
        ui/wrappers/screenapi.cpp \
        ui/wrappers/utils.cpp \
        ui/wrappers/wrappers.cpp

HEADERS += \
        ./ui/main_window/main_window.h \
        application.h \
        definitions.h \
        lib/libsources/api.h \
        lib/libsources/liblogman/log_manager.h \
        ui/device_prop/device_prop.h \
        ui/toast_message/toast_message.h \
        ui/wrappers/input_string/input_string.h \
        ui/wrappers/wrappers.h

FORMS += \
        ./ui/main_window/main_window.ui \
        ui/device_prop/device_prop.ui \
        ui/toast_message/toast_message.ui \
        ui/wrappers/input_string/input_string.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QMAKE_CXXFLAGS += -Wno-unused-command-line-argument -fpermissive -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include # -Wno-reorder

LIBS +=  -lsigc-2.0 \
         -lexplain \
         -lgio-2.0     # api::sys::get_file_icon_name

INCLUDEPATH += $$PWD \
               /usr/include/sigc++-2.0 \
               /usr/include/glib-2.0 \
               /usr/lib/x86_64-linux-gnu/glib-2.0/include \
               /usr/lib/x86_64-linux-gnu/sigc++-2.0/include \

RESOURCES += \
   res/res.qrc

#include(libcxx.inc)
#include(clangxx.inc)
