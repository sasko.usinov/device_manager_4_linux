//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#ifndef API_H
#define API_H

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <typeinfo>

#include "lib/libsources/liblogman/log_manager.h"

#define SHELL_PATH                  std::string("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin")
#define NAME_TO_STRING(var)         (#var)
#define SHOW_VAR_VALUE              std::cout << "Var" << #var << " = " << var << std::endl;

// #define classname                   std::string(typeid(this).name())

   //------------------------------------------------------------------------------------------------//
   // Backported api - more will be released
   //------------------------------------------------------------------------------------------------//
   namespace api
   {
      #define                       API_ERROR_IDX_NOT_FOUND             std::string::npos
      #define                       API_ERROR_INVALID_IDX               -1

      #define                       API_ERROR_FILE_WRITE                1
      #define                       API_ERROR_FILE_OPEN                 2
      #define                       API_ERROR_CHMOD_FAILURE             3

      #define                       nullchr                             '\0'


      namespace str
      {
         extern api::log_manager    g_out, g_err;

         std::string                get_file_name_from_url              (const std::string & path_name, const char & path_del = '/');
         size_t                     get_char_index                      (const char & ch, const size_t & count, const std::string & src, size_t pos_from = 0);
         size_t                     char_count                          (const char & ch, const std::string & src);
         size_t                     char_count                          (const char & ch, size_t pos_start, size_t pos_stop,  const std::string & src);
         std::string                range_copy                          (const size_t & start, const size_t & end, const std::string & src);
         size_t                     get_last_index_of                   (const char & ch, const std::string & src);
         std::string                load_from_file                      (const std::string & local_file);
         std::string                remove_char                         (const char & ch, const std::string & src);

         std::string                replace_char                        (const char & old_char, const char & new_char, std::string str);

         std::string                trim                                (const std::string & str);
         std::string                range_copy                          (const size_t & start, const size_t & end, const std::string & src);

         std::string                to_lower_case                       (std::string);
         std::string                to_lower_case                       (std::string &);

         bool                       left_string_compare                 (const std::string & src, const std::string & str);
         bool                       right_string_compare                (const std::string & src, const std::string & str);
         bool                       mid_string_compare                  (size_t index, const std::string & src, const std::string & str);

         std::string                to_upper_case                       (std::string);
         std::string                to_upper_case                       (std::string &);

         std::string                get_path_from_url                   (const std::string &);

         std::string                remove_left_chars                   (char ch, const std::string & src);

         std::string                remove_chars                        (const std::vector<char> & lst, std::string & src);
         std::vector<std::string>   url2list                            (const std::string & url);
         std::string                enclose_string                      (const std::string & enc, const std::string & src);
         bool                       check_for_special_chars             (const std::string & src);
         std::string                get_file_extension_from_path        (const std::string & path_name);
         std::string                remove_file_extension               (const std::string & path_name);
      }

      namespace fs
      {
         extern api::log_manager    g_out, g_err;

         std::string                generate_unique_filename            (const std::string & path, const std::string & prefix, int start_idx = 0, const char & sep = '.');
         bool                       path_exists                         (const std::string &file_path);
         bool                       save_to_file                        (const std::string & file_path, const std::string & content, bool append = false);
         std::string                load_from_file                      (const std::string & file_path);
         bool                       is_directory                        (const std::string & file_path);
         bool                       create_directory                    (const std::string & dir_path);

         std::vector<std::string>   get_file_list                        (const std::string & dir_path);
         std::vector<std::string>   get_file_list_via_shell             (const std::string & dir_path);

         void                       remove_non_dirs                     (const std::string & dir_path);
         void                       remove_all_except                   (const std::string & dir_path, const std::vector<std::string> & lst);
         int                        rename_file                         (const std::string & full_file_path, const std::string & new_name_no_path);
         int                        remove_file                         (const std::string & file_path);
         int                        symbolic_link                       (const std::string & source, const std::string & target);
      }

      namespace vct
      {
         extern api::log_manager    g_out, g_err;

         #define                    WITH_TERM_CHARS                     1
         #define                    WITH_NO_TERM_CHARS                  0

         std::vector <std::string>  to_vector_string_list               (const std::string & src, char t1, char t2 = '\0', bool include_line_term = true);
         std::vector <std::string>  to_vector_string_list               (const std::string & src, std::vector<char> & t, bool include_line_term = true);
         std::string                to_string                           (const std::vector<std::string> & list, char term = nullchr);
         size_t                     index_of                            (std::vector<std::string> & list, const std::string & str);
         size_t                     left_index_of                       (std::vector<std::string> & list, const std::string & str);
         size_t                     left_index_of                       (std::vector<std::string> & list, const std::string & str, const std::string & str_2);

         std::vector <std::string>  extract_section                     (std::vector<std::string> & list, const std::string & starting_string);
         std::vector <std::string>  extract_section                     (std::vector<std::string> & list, size_t start_idx);
      }

      namespace cns
      {
         extern api::log_manager    g_out, g_err;

         struct runstruct
         {
            std::string             tmp_dir;
            std::string             std_out;
            std::string             std_err;
            int                     exit_code;
            runstruct() :           tmp_dir                             ("/tmp"), exit_code(-1)   {}
         };

         runstruct                  exec                                (const std::string & cmd, const std::string & output_dir = "/tmp");
         runstruct                  exec_ext                            (const std::string & cmd, const std::string & output_dir = "/tmp");
         std::string                run                                 (const std::string & cmd);

         std::vector<std::string>   get_supported_terminals             ();
         std::string                get_preferred_terminal              ();
      }

      namespace sys
      {
         extern api::log_manager    g_out, g_err;

         inline std::string         get_env                             (const std::string & var) {char * out {getenv(var.c_str())}; return (out) ? out : std::string();}
         inline bool                vm_detected                         () {return !system("dmesg | grep \"Hypervisor detected\"");}
         std::string                read_link                           (const std::string & path);
         std::string                get_file_icon_name                  (const std::string & file_path);
         std::string                get_file_icon_path                  (const std::string & file_path, const std::vector<std::string> & icon_theme_paths, const std::string & icon_theme_name, const std::string & icon_label = "");

         std::vector<std::string>   get_library_loader_search_paths     ();
         void                       exec_background_cmd                 (const std::string & cmd, bool run_as_root = false);
      }

      namespace ini
      {
         extern api::log_manager    g_out, g_err;

         std::string                get_ini_value                       (const std::string & file, const std::string & section, const std::string & key);
         std::string                get_ini_value                       (const std::vector<std::string> & ini_content, const std::string & section, const std::string & key);

         size_t                     get_ini_index_of_key                (const std::string & file, const std::string & section, const std::string & key);
         size_t                     get_ini_index_of_key                (const std::vector<std::string> & ini_content, const std::string & section, const std::string & key);

         size_t                     get_ini_index_of_section            (const std::string & file, const std::string & section);
         size_t                     get_ini_index_of_section            (const std::vector<std::string> & ini_content, const std::string & section);

         bool                       write_to_ini                        (const std::string & file, const std::string & section, const std::string & key, const std::string & value);

         bool                       remove_ini_section                  (const std::string & file, const std::string & section);
      }
   }

#endif   // API_H
