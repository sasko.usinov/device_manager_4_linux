//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include <iostream>
#include <string>
#include <sstream>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <vector>

#include "log_manager.h"

api::log_manager glogger;

//-------------------------------------------------------------------------------//
// 
//-------------------------------------------------------------------------------//
//
//  Header File:   log_manager.h
//
//-------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager::log_manager() : m_n_tty_fd(-1), m_n_log_level(level_std), m_n_logging_enabled(true)
{   
   set_log_title(__func__);
   set_line_ending_term(LINE_BREAK);

   endl();
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager::log_manager(const int & level) : m_n_tty_fd(-1), m_n_log_level(level), m_n_logging_enabled(true)
{
   set_log_title(__func__);
   set_line_ending_term(LINE_BREAK);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager::log_manager(const std::string & path_to_tty) : m_s_tty_id(path_to_tty), m_n_tty_fd(-1), m_n_log_level(level_std), m_n_logging_enabled(true)
{
   set_log_title(__func__);
   set_line_ending_term(LINE_BREAK);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints str without a new line
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print(const std::string & str)
{
   write_log(m_n_log_level, str);

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints str with a new line
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::println(const std::string & str, bool add_extra_break)
{   
   endl();
   write_log(m_n_log_level, get_log_title(m_n_log_level) + get_time() + str);

   if (add_extra_break)
   {
      endl();
      endl();
   }

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints num with a new line
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::println(const signed long long & num)
{
   endl();
   write_log(m_n_log_level, get_log_title(m_n_log_level) + get_time() + std::to_string(num) + m_s_eolch);

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints out the num without a new line
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print(const signed long long & num)
{
   write_log(m_n_log_level, std::to_string(num));

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints a new line
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::endl()
{
   write_log(m_n_log_level, m_s_eolch);

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints the 'c' repeatedly for 'len'th times
//                The idea is to make the output look like a horizontal rule
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print_border(const char & c, const int & len)
{
   endl();

   for (int i = 0; i < len; i++)
      write_log(m_n_log_level, std::string(1, c));

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints title surrounded by the 'c' character
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print_header(const std::string &title, const char & c)
{
// print("\n");
   print_border(c, title.length()).endl().print(title).print_border(c, title.length());
   print("\n");

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints a string to a tty
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print_to_tty(const std::string & str)
{
   if (!m_s_tty_id.length())         return (*this);
   if (!m_n_logging_enabled)         return (*this);

   m_n_tty_fd = open(m_s_tty_id.c_str(), O_RDWR | (O_APPEND | O_CREAT), S_IRWXU);

   if (m_n_tty_fd > -1)
   {
      write(m_n_tty_fd, str.c_str(), str.length());
      close(m_n_tty_fd);
   }
   else
      write_log(level_err,"Error, unable to write to tty: " + m_s_tty_id + " for some reason.\n\n");

   return (*this);
}


//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints a string to a file
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print_to_file(const std::string & str)
{
   if (!m_s_log_file_path.length())  return (*this);
   if (!m_n_logging_enabled)         return (*this);

   int fd = open(m_s_log_file_path.c_str(), O_RDWR | (O_APPEND | O_CREAT), S_IRWXU);

   if (fd > -1)
   {
      write(fd, str.c_str(), str.length());
      close(fd);
   }
   else
      write_log(level_err,"Error, unable to write to log file: " + m_s_log_file_path + ", check path permissions");

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Prints a string to a file
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
api::log_manager & api::log_manager::print_to_file(const std::string & str, const std::string & path)
{
   if (!m_s_log_file_path.length())  return (*this);
   if (!m_n_logging_enabled)         return (*this);

   int fd = open(path.c_str(), O_RDWR | (O_APPEND | O_CREAT), S_IRWXU);

   if (fd > -1)
   {
      write(fd, str.c_str(), str.length());
      close(fd);
   }
   else
      write_log(level_err,"Error, unable to write to log file: " + m_s_log_file_path + ", check path permissions");

   return (*this);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
std::string api::log_manager::get_log_title(const int & level)
{
   return (level == level_std) ? "(OK) [" + m_s_session_title + "] " : "(ER) [" + m_s_session_title + "] ";
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
//  ToDo:         n/a
//
//  Notes:        n/a
//
//  Returns:      n/a
//
//-------------------------------------------------------------------------------//
void api::log_manager::write_log(int level, const std::string & msg)
{
   if (!m_n_logging_enabled)         return;

   write(level, msg.c_str(),     msg.length());

   print_to_tty(msg);
   print_to_file(msg);
}

//-------------------------------------------------------------------------------//
// class
//-------------------------------------------------------------------------------//
//
//  ToDo:         n/a
//
//  Notes:        Returns the system time
//
//  Returns:      n/a
//
//-------------------------------------------------------------------------------//
std::string api::log_manager::get_time()
{

   std::string    hours, minutes, seconds, day, month, year, out;

   time_t tt      =  time(nullptr);
   struct tm tm   =  *localtime(&tt);

   year           =  std::to_string(tm.tm_year + 1900);

   month          = ((tm.tm_mon + 1) < 10)      ? ("0" + std::to_string(tm.tm_mon + 1))   : std::to_string(tm.tm_mon + 1);
   day            = (tm.tm_mday < 10)           ? ("0" + std::to_string(tm.tm_mday))      : std::to_string(tm.tm_mday);
   hours          = (tm.tm_hour < 10)           ? ("0" + std::to_string(tm.tm_hour))      : std::to_string(tm.tm_hour);
   minutes        = (tm.tm_min < 10)            ? ("0" + std::to_string(tm.tm_min))       : std::to_string(tm.tm_min);
   seconds        = (tm.tm_sec < 10)            ? ("0" + std::to_string(tm.tm_sec))       : std::to_string(tm.tm_sec);
   out            = (!m_s_session_title.length())   ? std::string()                           : "(" + day +  "-" + month + "-" + year + "@" + hours + ":" + minutes + ":" + seconds + "): ";

   return         out;
}

