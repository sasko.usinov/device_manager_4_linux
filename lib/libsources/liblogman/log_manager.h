//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#ifndef LOGMAN_H
#define LOGMAN_H

#include <iostream>
#include <string>

namespace api
{ 
   //-------------------------------------------------------------------------------------------------------
   // log_manager - Message Logger
   //-------------------------------------------------------------------------------------------------------

   class log_manager
   {
      #define                                   LINE_BREAK                 "\n"

      //-------------------------------------------------------------------------------------------------------
      // public
      //-------------------------------------------------------------------------------------------------------
      public:

         //-------------------------------------------------------------------------------------------------------
         // methods
         //-------------------------------------------------------------------------------------------------------
         typedef enum
         {level_std = 1, level_err,}            levels;

         explicit                               log_manager                ();
         explicit                               log_manager                (const int & level);
         explicit                               log_manager                (const std::string & path_to_tty);

         log_manager &                          print                      (const std::string & str);
         log_manager &                          print                      (const signed long long &num);

         log_manager &                          println                    (const std::string & str, bool add_extra_break = false);
         log_manager &                          println                    (const signed long long & num = 0);

         log_manager &                          endl                       ();

         log_manager &                          print_border               (const char & c, const int & len = 50);
         log_manager &                          print_header               (const std::string &title, const char & c);

         inline void                            redirect_to_tty            (const std::string & path_to_tty)   {m_s_tty_id = path_to_tty;}
         inline void                            set_log_title              (const std::string &title)          {m_s_session_title = title;}
         inline void                            set_log_level              (int level)                         {m_n_log_level = level;}

         std::string                            get_log_title              (const int & level = level_std);

         //-------------------------------------------------------------------------------------------------------
         // inline
         //-------------------------------------------------------------------------------------------------------
         inline void                            enable_logging             () {m_n_logging_enabled = 1;}
         inline void                            disable_logging            () {m_n_logging_enabled = 0;}
         inline void                            set_line_ending_term       (const std::string & eol) {m_s_eolch = eol;}
         inline void                            set_log_file               (const std::string & path){m_s_log_file_path = path;}
         inline void                            set_enabled                (int status) {m_n_logging_enabled = status;}
         inline bool                            get_logging_status         () {return m_n_logging_enabled;}

         //-------------------------------------------------------------------------------------------------------
         // ops
         //-------------------------------------------------------------------------------------------------------
         inline log_manager &                   operator<<                 (const std::string & str) {print(str); return *this;}
         inline log_manager &                   operator<<                 (const signed long long & val) {print(val); return *this;}
         inline log_manager &                   operator<<                 (const log_manager &) {endl(); return *this;}

      //-------------------------------------------------------------------------------------------------------
      // private
      //-------------------------------------------------------------------------------------------------------
      private:

         //-------------------------------------------------------------------------------------------------------
         // vars
         //-------------------------------------------------------------------------------------------------------
         std::string                            m_s_tty_id;
         int                                    m_n_tty_fd;
         int                                    m_n_log_level;
         int                                    m_n_logging_enabled;
         std::string                            m_s_session_title;
         std::string                            m_s_eolch;
         std::string                            m_s_log_file_path;

         //-------------------------------------------------------------------------------------------------------
         // methods
         //-------------------------------------------------------------------------------------------------------
         std::string                            get_time                   ();

         log_manager &                          print_to_tty               (const std::string & str);
         log_manager &                          print_to_file              (const std::string & str);
         log_manager &                          print_to_file              (const std::string & str, const std::string & path);

         void                                   write_log                  (int level, const std::string & msg);

   }; 
}

extern api::log_manager glogger;

#endif // LOGMAN_H

