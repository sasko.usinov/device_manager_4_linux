//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "api.h"
#include "liblogman/log_manager.h"

#include <iostream>
#include <string>
#include <sstream>

#include <sys/klog.h>
#include <sys/stat.h>

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <algorithm>

api::log_manager api::str::g_out, api::str::g_err(api::log_manager::level_err);

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the index of the last occurance of ch in src
//
// Returns:       See desciption
//
//-------------------------------------------------------------------------------//
size_t api::str::get_last_index_of(const char & ch, const std::string & src)
{
   size_t out        {API_ERROR_IDX_NOT_FOUND};

   for (size_t i = 0; i < src.length(); i++)
      (src[i] == ch) ? out = i : out;

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Removes ch from src
//
// Returns:       The modified string
//
//-------------------------------------------------------------------------------//
std::string api::str::remove_char(const char & ch, const std::string & src)
{
   std::string out   {};

   for (std::size_t i = 0; i < src.length(); i++)
      (src[i] != ch) ? out += src[i] : out;

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the contents of local file
//
// Returns:       A string containing the contents of the loaded file
//
//------------------------------------------------------------------------------//
std::string api::str::load_from_file(const std::string & local_file)
{
   std::ifstream ifs (local_file.c_str());

   if (!ifs.is_open())  return std::string();

   std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
   ifs.close();

   return str;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Trims the string on both ends
//
// Returns:       The modified string
//
//------------------------------------------------------------------------------//
std::string api::str::trim(const std::string & src)
{
   size_t   start_copy  (0);
   size_t   end_copy    (src.length() - 1);

   while (src[start_copy]  < 33  || src[start_copy] > 126)  start_copy++;
   while (src[end_copy]    < 33  || src[end_copy] > 126)    end_copy--;

   return api::str::range_copy(start_copy, end_copy, src);
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Replaces all occurences of old_char with new_char in src
//
// Returns:       A copy of the modified string
//
//-------------------------------------------------------------------------------//
std::string api::str::replace_char(const char & old_char, const char & new_char, std::string str)
{
   for (size_t i = 0; i < str.length(); i++)
      (str[i] == old_char) ? str[i] = new_char : str[i];

   return str;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Copies characaters from start to end in src
//
// Returns:       The copied characters
//
//-------------------------------------------------------------------------------//
std::string api::str::range_copy(const size_t & start, const size_t & end, const std::string & src)
{
   std::string out;

   if (start   >= src.length())                    return out;
   if (end     >= src.length())                    return out;
   if (start   >  end)                             return out;

   if (static_cast<long signed int>(start)  <  0)  return out;
   if (static_cast<long signed int>(end)    <  0)  return out;

   for (size_t i = start; i <= end; i++)
   {
      if (i > (src.length() - 1))
         break;

      out = out + src[i];
   }

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         It checks whether string exists in src at the start of src
//
// Returns:       True if so otherwise false
//
//-------------------------------------------------------------------------------//
bool api::str::left_string_compare(const std::string & src, const std::string & str)
{
   if (str.size() > src.size())
      return false;

   for (size_t i = 0; i < src.size(); i++)
   {
      if (i >= str.size())
         break;

      if (str[i] != src[i])
         return false;
   }

   return true;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         It checks whether target exists in src at the end of src
//
// Returns:       True if so otherwise false
//
//-------------------------------------------------------------------------------//
bool api::str::right_string_compare(const std::string & src, const std::string & str)
{
   if (str.size() > src.size())
      return false;

   size_t start = src.size() - str.size();

   for (size_t i = start; i < src.size(); i++)
   {
      if (str[i - start] != src[i])
         return false;
   }

   return true;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         It checks whether target exists in src at position specified
//                by index
//
// Returns:       True if so otherwise false
//
//-------------------------------------------------------------------------------//
bool api::str::mid_string_compare(size_t index, const std::string & src, const std::string & str)
{
   if (str.size() > src.size())     return false;

   if (index >= src.length())       return false;

   for (size_t i = 0; i < str.size(); i++)
   {
      if (src[i + index] != str[i])
         return false;
   }

   return true;
}

//-------------------------------------------------------------------------------//
// Type:          method
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
std::string api::str::to_lower_case(std::string str)
{
   for (size_t i = 0; i < str.size(); i++)
      str[i] = std::tolower(str[i], std::locale());

   return str;
}

//-------------------------------------------------------------------------------//
// Type:          method
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
std::string api::str::to_lower_case(std::string & str)
{
   for (size_t i = 0; i < str.size(); i++)
      str[i] = std::tolower(str[i], std::locale());

   return str;
}


//-------------------------------------------------------------------------------//
// Type:          method
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
std::string api::str::to_upper_case(std::string str)
{
   for (size_t i = 0; i < str.size(); i++)
      str[i] = std::toupper(str[i], std::locale());

   return str;
}

//-------------------------------------------------------------------------------//
// Type:          method
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//--------------------------------------------------------------------------------------------------------------------------------------------//
std::string api::str::to_upper_case(std::string & str)
{
   for (size_t i = 0; i < str.size(); i++)
      str[i] = std::toupper(str[i], std::locale());

   return str;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          ...
//
// Notes:         Returns the name of the file only if path_name contained an absolute or relative path
//
// Returns:       The file's name
//
//-------------------------------------------------------------------------------//
std::string api::str::get_file_name_from_url(const std::string & path_name, const char & path_del)
{
   if (!api::str::char_count(path_del, path_name) || path_name == std::string(1, path_del))
      return path_name;

   return api::str::range_copy (api::str::get_last_index_of(path_del, path_name) + 1, path_name.size() - 1, path_name);
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the index of the count-th ch in src
//
// Returns:       See desciption
//
//-------------------------------------------------------------------------------//
size_t api::str::get_char_index(const char & ch, const size_t & count, const std::string & src, size_t pos_from)
{
   size_t out     {API_ERROR_IDX_NOT_FOUND};
   size_t counter {0};

   for (size_t idx = pos_from; idx < src.length(); idx++)
   {
      if (src.c_str()[idx] == ch)
      {
          out = idx;
          counter++;
      }

      if (counter == count) return out;
   }

   return API_ERROR_IDX_NOT_FOUND;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the number of times ch appears in src
//
// Returns:       See description
//
//-------------------------------------------------------------------------------//
size_t api::str::char_count(const char & ch, const std::string & src)
{
   size_t counter {0};

   for (size_t i = 0; i < src.length(); i++)
      (src[i] == ch) ? counter++ : counter;

   return counter;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns how many times ch appears in src
//
// Returns:       See description
//
//-------------------------------------------------------------------------------//
size_t api::str::char_count(const char & ch, size_t pos_start, size_t pos_stop, const std::string & src)
{
   size_t counter {0};

   (pos_stop >= src.length()) ? pos_stop = src.length() - 1 : pos_stop;

   for (size_t i = pos_start; i <= pos_stop; i++)
      (src[i] == ch) ? counter++ : counter;

   return counter;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Removes all chars matching ch from the start of the string
//
// Returns:       The modified string
//
//-------------------------------------------------------------------------------//
std::string api::str::remove_left_chars(char ch, const std::string & src)
{
   size_t idx = 0;

   while (src[idx++] == ch);

   return range_copy(idx - 1, src.length() - 1, src);
}

//-------------------------------------------------------------------------------//
// namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Removes any char that's in lst from src
//
// Returns:       The modified string
//
//------------------------------------------------------------------------------//
std::string api::str::remove_chars(const std::vector<char> & lst, std::string & src)
{
   for (std::size_t i = 0; i < src.length(); i++)
   {
      for (size_t x = 0; x < lst.size(); x++)
      {
         if (src[i] == lst[x])
            src.erase(i, 1);
      }
   }

   return src;
}

//-------------------------------------------------------------------------------//
// namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Creates a vector list of strings from url where each /string/
//                in url is added to the list
//
// Returns:       The vector list
//
//------------------------------------------------------------------------------//
std::vector<std::string> api::str::url2list(const std::string & url)
{
   std::vector<std::string>   out;
   std::string                tmp;

   for (size_t i = 0; i < url.size(); i++)
   {
      if ((url[i] == '/' || url[i] == '\\') && i > 0)
      {
         out.push_back(tmp);
         tmp.clear();
      }

      tmp += url[i];
   }

   out.push_back(tmp);
   return out;
}

//-------------------------------------------------------------------------------//
// namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the path of full_file_path without the file name
//
// Returns:       A string containing the file path
//
//------------------------------------------------------------------------------//
std::string api::str::get_path_from_url(const std::string & full_file_path)
{
   return    api::str::range_copy(0,
             api::str::get_char_index('/', api::str::char_count('/', full_file_path), full_file_path) - 1,
             full_file_path);
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Encloses src with the enc
//
// Returns:       The modified string
//
//-------------------------------------------------------------------------------//
std::string api::str::enclose_string(const std::string & enc, const std::string & src)
{
   return enc + src + enc;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Checks to see whether src has special chars
//
// Returns:       true if so otherwise false
//
//-------------------------------------------------------------------------------//
bool api::str::check_for_special_chars(const std::string & src)
{
   for (size_t i = 0; i < src.length(); i++)
   {
       if (src[i] >= 33 && src[i] <= 47 && src[i] != 45)    return true;
       if (src[i] >= 58 && src[i] <= 64)                    return true;
       if (src[i] >= 91 && src[i] <= 94)                    return true;
       if (src[i] == 96)                                    return true;
       if (src[i] > 123)                                    return true;
   }

   return false;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the file extension contained in path_name
//
// Returns:       See description
//
//-------------------------------------------------------------------------------//
std::string api::str::get_file_extension_from_path(const std::string & path_name)
{
   size_t dot_pos(api::str::get_last_index_of('.', path_name));

   if (dot_pos == 0)                         return path_name;
   if (dot_pos == path_name.length() - 1)    return path_name;
   if (dot_pos == API_ERROR_IDX_NOT_FOUND)   return std::string();

   if (dot_pos != API_ERROR_IDX_NOT_FOUND)
      return api::str::range_copy(dot_pos + 1, path_name.length() - 1, path_name);

   return path_name;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Removes the file extenstion from path_name
//
// Returns:       See desciption
//
//-------------------------------------------------------------------------------//
std::string api::str::remove_file_extension(const std::string & path_name)
{
   std::size_t last_dot = api::str::get_last_index_of('.', path_name);

   if (last_dot == 0)                     return path_name;
   if (last_dot == std::string::npos)     return path_name;

   return api::str::range_copy(0, last_dot - 1, path_name);
}
