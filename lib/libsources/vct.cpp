//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#include "api.h"
#include "liblogman/log_manager.h"

#include <iostream>
#include <string>
#include <sstream>

#include <sys/klog.h>
#include <sys/stat.h>

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <algorithm>

api::log_manager api::vct::g_out, api::vct::g_err(api::log_manager::level_err);

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Converts a vector string list to std::string
//
// Returns:       The converted string
//
//-------------------------------------------------------------------------------//
std::string api::vct::to_string(const std::vector<std::string> &list, char term)
{
   std::string out;

   for (long unsigned int i = 0; i < list.size(); i++)
      out += (term != '\0') ? (list[i] + std::string(1, term)) : list[i];

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Creates a vector list of strings out of src where t is a list of terminator
//                chars where the string needs to be split so the src gets split every time
//                any of the chars in t is found
//
// Returns:       The vector list
//
//------------------------------------------------------------------------------//
std::vector<std::string> api::vct::to_vector_string_list(const std::string & src, std::vector<char> & t, bool include_line_term)
{
   std::string                buff;
   std::vector <std::string>  out;

   for (size_t i = 0; i < src.length(); i++)
   {
      buff += src[i];

      for (size_t x = 0; x < t.size(); x++)
      {
         if (src[i] == t[x])
         {
            out.push_back(include_line_term ? buff : api::str::range_copy(0, buff.length() - 2, buff));
            buff.clear();
         }
      }
   }

   out.push_back(buff);
   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Creates a vector list of strings out of str if str contains
//                the t1 or t2 char.
//
// Returns:       The vector list
//
//-------------------------------------------------------------------------------//
std::vector<std::string> api::vct::to_vector_string_list(const std::string & src, char t1, char t2, bool include_line_term)
{
   std::string                buff;
   std::vector <std::string>  out;

   for (long unsigned int i = 0; i < src.size(); i++)
   {
      if (include_line_term)
         buff += src[i];
      else
      {
         if (src[i] != t1 && src[i] != t2)
            buff += src[i];
      }

      if (i == src.length() - 1 || src[i] == t1 || src[i] == t2)
      {
          out.push_back(buff);
          buff.clear();
      }
   }

   return out;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the index of str in list if str exists in list
//
// Returns:       The position index of str in list or API_ERROR_IDX_NOT_FOUND if
//                it does not exist
//
// To Do:         n/a
//
//-------------------------------------------------------------------------------//
size_t api::vct::index_of(std::vector<std::string> & list, const std::string & str)
{
   for (size_t i = 0; i < list.size(); i++)
   {
      if (list[i].size() == str.size())
      {
         if (list[i] == str)
            return i;
      }
   }

   return API_ERROR_IDX_NOT_FOUND;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the index of str in list if str exists in any of the strings in lst
//                at the start of the string
//
// Returns:       The position index of str in list or -1 if it does not exist
//
// To Do:         n/a
//
//-------------------------------------------------------------------------------//
size_t api::vct::left_index_of(std::vector<std::string> & list, const std::string & str)
{
   for (size_t i = 0; i < list.size(); i++)
   {
      if (list[i].size() >= str.size())
      {
         if (list[i].substr(0, str.size()) == str)
            return i;
      }
   }

   return API_ERROR_IDX_NOT_FOUND;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Returns the index of str in list if str exists in any of the strings in lst
//                at the start of the string along with str_2 in the next item
//
// Returns:       The position index of str in list or -1 if it does not exist
//
// To Do:         n/a
//
//-------------------------------------------------------------------------------//
size_t api::vct::left_index_of(std::vector<std::string> & list, const std::string & str, const std::string & str_2)
{
   for (size_t i = 0; i < list.size(); i++)
   {
      if (list[i].size() >= str.size())
      {
         if (list[i].substr(0, str.size()) == str)
         {
            if (i < list.size() - 2)
            {
               if (list[i + 1].substr(0, str_2.size()) == str_2)
                  return i;
            }
         }
      }
   }

   return API_ERROR_IDX_NOT_FOUND;
}

//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Copies everything from the list starting from the position that
//                left matches starting_string to the next empty string
//
// Returns:       A vector string containing the copied strings
//
// To Do:         n/a
//
//-------------------------------------------------------------------------------//
std::vector<std::string> api::vct::extract_section(std::vector<std::string> & list, const std::string & starting_string)
{
   std::vector<std::string> out;

   for (size_t i = 0; i < list.size(); i++)
   {
      if (list[i].size() >= starting_string.size())
      {
         if (list[i].substr(0, starting_string.size()) == starting_string)
         {
            while (api::str::trim(list[i]).size())
               out.push_back(list[i++]);

            break;
         }
      }
   }

   return out;
}


//-------------------------------------------------------------------------------//
// Type:          namespace
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         Copies everything from start_idx to the next empty string
//
// Returns:       A vector string containing the copied strings
//
// To Do:         n/a
//
//-------------------------------------------------------------------------------//
std::vector<std::string> api::vct::extract_section(std::vector<std::string> & list, size_t start_idx)
{
   std::vector<std::string> out;

   for (; start_idx < list.size(); start_idx++)
   {
      if (!api::str::trim(list[start_idx]).size())
         break;

      out.push_back(list[start_idx]);
   }

   return out;
}

