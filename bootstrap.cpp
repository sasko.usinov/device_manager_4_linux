//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//

#include <unistd.h>
#include <sys/types.h>

#include "application.h"
#include "ui/main_window/main_window.h"
#include "ui/wrappers/wrappers.h"

#include <QApplication>

//-------------------------------------------------------------------------------//
// Type:          global
//-------------------------------------------------------------------------------//
//
// ToDo:          n/a
//
// Notes:         n/a
//
// Returns:       n/a
//
//-------------------------------------------------------------------------------//
int __init__(int argc, char *argv[])
{
   QApplication a(argc, argv);

   app::config_env() ? sysapi::quit(app::last_err.get_error_str(), app::last_err.get_error_code()) : app::print_config();

   if (RUN_EULA)
   {
      std::string eula_acc_file = app::data::app_settings_dir + "/" + app::about::app_name + "_" + app::about::app_version + "_" + api::str::replace_char(32, '_', app::about::app_build_date) + "_" + api::str::replace_char(':', '_', app::about::app_build_time) + ".eula";

      if (api::fs::load_from_file(eula_acc_file) != TXT_EULA_ACCEPTED)
      {
         if (messages::show_question(nullptr, app::about::app_friendly_name.c_str(), (TXT_EULA_CONTENT + std::string("\n\nDo you accept this?")).c_str(), messages::BTNS_QST_YESNO, messages::ICON_QUESTION) == messages::BTN_NO)
            exit(0);

         api::fs::save_to_file(eula_acc_file, TXT_EULA_ACCEPTED);
      }
   }

   //-------------------------------------------------------------------------------//
   // ...
   //-------------------------------------------------------------------------------//
   app::init_watch_list();

   for (size_t i = 0; i < app::data::watch_list.size(); i++)
   {
      app::data::thread_list.push_back(std::thread(app::t_device_monitor, app::data::watch_list[i]));
      app::data::thread_list[app::data::thread_list.size() - 1].detach();
   }

   __run__
}
