//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//
//
//    Copyright © 2019 Sasko Usinov
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights to
//    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    the Software, and to permit persons to whom the Software is furnished to do so,
//    subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//-----------------------------------------------------------------------------------------//
//    Distributed under the MIT License - https://opensource.org/licenses/MIT
//-----------------------------------------------------------------------------------------//


#ifndef APPLICATION_H
#define APPLICATION_H

#include <sigc++-2.0/sigc++/sigc++.h>

#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include "./lib/libsources/liblogman/log_manager.h"
#include "./lib/libsources/api.h"

#include "definitions.h"
#include "log_macros.h"

//------------------------------------------------------------------------------------------------//
// enums
//------------------------------------------------------------------------------------------------//
   enum err_codes
   {
      OP_FAIL,
      OP_SUCCESS,
      OP_NO_ACTION_TAKEN
   };

   enum action_types
   {
      ACT_DISABLE_DEVICE,
      ACT_ENABLE_DEVICE
   };

   enum scsi_action_types
   {
      ACTION_SCSI_NONE,
      ACTION_SCSI_DISABLE_DEVICE,
      ACTION_SCSI_RESCAN_DEVICES
   };

   enum script_types
   {
      SCRIPT_TYPE_PCI,
      SCRIPT_TYPE_SCSI,
      SCRIPT_TYPE_USB,
   };
//-------------------------------------------------------------------------------//
// end enums
//-------------------------------------------------------------------------------//

//------------------------------------------------------------------------------------------------//
// externs
//------------------------------------------------------------------------------------------------//
   extern api::log_manager             write_out,
                                       write_err;
//------------------------------------------------------------------------------------------------//
// end externs
//------------------------------------------------------------------------------------------------//

//------------------------------------------------------------------------------------------------//
// structs
//------------------------------------------------------------------------------------------------//
   struct pci_device_struct
   {
      inline                           pci_device_struct() :
                                       device_enabled(true), data(nullptr) {}

      std::string                      kernel_module;
      std::string                      sysfs_id;
      std::string                      device_type;
      std::string                      device_desc;
      std::string                      dev_type_id;
      std::string                      dev_desc_vid_did;
      std::string                      boot_script_path;
      std::string                      parent_ref;

      bool                             device_enabled;

      std::vector<std::string>         prop;
      void *                           data;
   };

   struct boot_entry_struct
   {
      std::string                      boot_script_path;
      std::string                      device_desc;
      std::string                      entry_line;
   };

   struct scsi_device_struct
   {
      std::string                      scsi_idx;
      std::string                      hba_number;
      std::string                      product_info;
      std::string                      product_desc;
      std::vector<std::string>         props;
      std::string                      boot_script_path;
   };

   struct usb_device_struct
   {
      inline                           usb_device_struct () :
                                       hwi_prop_start_idx(1), device_enabled(false)  {}

      std::string                      bus_idx;
      std::string                      device_idx;
      std::string                      vid;
      std::string                      did;
      std::string                      vid_did;
      std::string                      vid_did_processed;
      std::string                      device_type;
      std::string                      hub_info;
      std::string                      actual_dev_details;
      long                             hwi_prop_start_idx;
      bool                             device_enabled;
      std::string                      title;
      std::string                      boot_script_path;
      std::string                      sysfs_id;
   };

   //-------------------------------------------------------------------------------//
   // Used for error values
   //-------------------------------------------------------------------------------//
   struct error_structure
   {
      public:
         inline                        error_structure                     () : err_code(EXIT_SUCCESS) {}

         inline void                   set_error                           (std::string msg, int code)
                                                                           {err_message = msg; err_code = code; if (msg.length()) elog(msg);}
         inline std::string            get_error_str                       () {return err_message;}
         inline int                    get_error_code                      () {return err_code;}

      private:
         std::string                   err_message;
         int                           err_code;
   };

   //-------------------------------------------------------------------------------//
   // Used for file monitoring
   //-------------------------------------------------------------------------------//
   struct file_watcher_data
   {
      size_t                           size;
      signed long                      idx;
      signed long                      length;
      int                              file_descriptor;
      int                              watch_descriptor;
      char                             buffer[WATCHER_BUFF_SIZE];
      std::string                      dirpath;

      inline file_watcher_data         (const std::string & path = "/dev") :
      size                             (WATCHER_BUFF_SIZE),
      idx                              (0),
      length                           (0),
      file_descriptor                  (0),
      watch_descriptor                 (0),
      buffer                           (),
      dirpath(path)                    {}
   };
//-------------------------------------------------------------------------------//
// end structs
//-------------------------------------------------------------------------------//


//------------------------------------------------------------------------------------------------//
// namespaces
//------------------------------------------------------------------------------------------------//
   namespace app
   {
      //------------------------------------------------------------------------------------------------//
      // externs
      //------------------------------------------------------------------------------------------------//
      extern std::vector<pci_device_struct>                                pci_list;
      extern std::vector<boot_entry_struct>                                boot_list;
      extern std::vector<scsi_device_struct>                               scsi_list;
      extern std::vector<usb_device_struct>                                usb_list;

      extern std::vector<pci_device_struct>                                pci_search_list;
      extern std::vector<scsi_device_struct>                               scsi_search_list;
      extern std::vector<usb_device_struct>                                usb_search_list;

      extern error_structure                                               last_err;

      //------------------------------------------------------------------------------------------------//
      // sigc
      //------------------------------------------------------------------------------------------------//
      extern sigc::signal <void>                                           sigc_clear_device_list;
      extern sigc::signal <void, std::vector<pci_device_struct> & >        sigc_load_pci_list;
      extern sigc::signal <void, std::vector<scsi_device_struct> &>        sigc_load_scsi_list;
      extern sigc::signal <void, std::vector<usb_device_struct> &>         sigc_load_usb_list;

      //------------------------------------------------------------------------------------------------//
      // we need to keep an eye on the connection and disconnect to prevent freezes
      //------------------------------------------------------------------------------------------------//

      extern sigc::signal <void, const std::string &>                      sigc_file_added;
      extern sigc::signal <void, const std::string &>                      sigc_file_removed;
      extern sigc::signal <void, const std::string &>                      sigc_dev_query_end;

      extern sigc::connection                                              sigc_conn_file_added;
      extern sigc::connection                                              sigc_conn_file_removed;

      //------------------------------------------------------------------------------------------------//
      // funtions and classes
      //------------------------------------------------------------------------------------------------//
      void                                load_pci_device_list             ();
      void                                load_scsi_device_list            ();
      void                                load_usb_device_list             ();

      std::string                         query_device                     (const std::string & dev_node);

      void                                search_for_device                (const char * dev_str);

      std::vector<std::string>            get_device_prop                  (const std::string & sysfs_id);

      void                                t_device_monitor                 (const std::string & path);

      int                                 config_env                       ();
      void                                print_config                     ();

      inline bool                         can_access                       (const std::string & app) {return !system(std::string("bash -c \"" + SHELL_PATH + " && which " + app + " > /dev/null\"").c_str());}

      void                                init_watch_list                  ();

      void                                show_notification                (const std::string & title, const std::string & msg, int delay);

      inline bool                         lshw_found                       () {return can_access("lshw");}
      inline bool                         hwinfo_found                     () {return can_access("hwinfo");}
      inline bool                         lsusb_found                      () {return can_access("lsusb");}
      inline bool                         lspci_found                      () {return can_access("lspci");}

      namespace about
      {
         const std::string                app_name                         {"dm4l"};
         const std::string                app_friendly_name                {TXT_WINDOW_TITLE};
         const std::string                app_desc                         {TXT_TAG_LINE};
         const std::string                app_version                      {"0.01"};
         const std::string                app_build_date                   {__DATE__};
         const std::string                app_build_time                   {__TIME__};
         const std::string                app_author                       {"Sasko Usinov"};
         const std::string                app_contact                      {"neolinux.tech@optusnet.com.au"};
         const std::string                app_src                          {"https://gitlab.com/neolinux.tech/device-manager-4-linux"};
         const std::string                app_website                      {"neolinux.tech" };
      }

      namespace data
      {
         extern std::string               app_log_dir;
         extern std::string               app_home_dir;
         extern std::string               app_settings_dir;
         extern std::string               app_scripts_dir;
         extern std::string               app_backup_dir;

         extern file_watcher_data *       fwd;
         extern bool                      exiting;
         extern std::vector<std::string>  watch_list;
         extern std::vector<std::thread>  thread_list;
      }
   }
//-------------------------------------------------------------------------------//
// namespaces end
//-------------------------------------------------------------------------------//

#endif
