#ifndef PRIVATE_LOGGER
#define PRIVATE_LOGGER   

   #ifndef classname
//     #define classname                   std::string(typeid(this).name())
   #endif

   #ifndef plog
      #define plog(val)                   m_cout << "...[ OK-"  << PROJECT_ID << "]: " << std::string(__FILE__) + ", " + std::string(__func__) + std::string("()[") + std::to_string(__LINE__) + "]: " << val << "\n"
   #endif

   #ifndef perr
      #define perr(val)                   m_cerr << "...[ ERR-" << PROJECT_ID << "]: " << std::string(__FILE__) + ", " + std::string(__func__) + std::string("()[") + std::to_string(__LINE__) + "]: " << val << "\n"
   #endif

   #ifndef clog
      #define clog(val)                   write_out << "...[ OK-"  << PROJECT_ID << "]: " << std::string(__FILE__) + ", " + std::string(__func__) + std::string("()[") + std::to_string(__LINE__) + "]: " << val << "\n"
   #endif

   #ifndef elog
      #define elog(val)                   write_err << "...[ ERR-" << PROJECT_ID << "]: " << std::string(__FILE__) + ", " + std::string(__func__) + std::string("()[") + std::to_string(__LINE__) + "]: " << val << "\n"
   #endif

#endif
